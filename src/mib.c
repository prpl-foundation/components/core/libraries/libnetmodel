/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <amxs/amxs.h>
#include <netmodel/client.h>
#include <netmodel/common_api.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/mib.h>
#include "netmodel_mib.h"

#define ME "mib"
#define string_empty(x) ((x == NULL) || (*x == '\0'))
#define MIB_RETRY_TIMEOUT 10000

typedef struct _ctx_htable_entry {
    amxc_htable_it_t it;
    amxs_sync_ctx_t* ctx;
    amxb_subscription_t* subscription_interface_path;
    amxb_subscription_t* subscription_lower_layers;
    amxb_subscription_t* subscription_obj_a;
    amxb_bus_ctx_t* plugin_bus;
    char* netmodel_intf;
    char* interface_path;
    netmodel_mib_t* mib;
    amxc_llist_t lower_layers;
    bool is_upper_layer;
    amxp_timer_t* timer;
    uint32_t retry_timeout;
} ctx_htable_entry_t;

typedef struct {
    amxc_llist_it_t it;
    netmodel_query_t* query;
    char* lower_layer;
    char* netmodel_intf;
    ctx_htable_entry_t* entry;
    bool close_query;
} lower_layer_t;

static amxc_htable_t* ctx_htable = NULL;

static void retry_timer_cb(amxp_timer_t* timer, void* priv);

static bool flag_is_not_set(netmodel_mib_t* mib, uint32_t flag) {
    return ((mib->flags & flag) == 0);
}

static bool make_sync_context(ctx_htable_entry_t* entry, const char* obj_a, const char* obj_b) {
    amxs_status_t ret = amxs_status_unknown_error;

    SAH_TRACEZ_INFO(ME, "new sync context %s and %s", obj_a, obj_b);

    ret = amxs_sync_ctx_new(&entry->ctx, obj_a, obj_b, AMXS_SYNC_ONLY_A_TO_B);
    if(flag_is_not_set(entry->mib, NETMODEL_MIB_ONLY_MY_PARAMETERS)) {
        ret |= amxs_sync_ctx_add_new_copy_param(entry->ctx, "Status", "Status_ext", AMXS_SYNC_DEFAULT);
        ret |= amxs_sync_ctx_add_new_copy_param(entry->ctx, "Enable", "Enable", AMXS_SYNC_DEFAULT);
        ret |= amxs_sync_ctx_add_new_copy_param(entry->ctx, "Name", "Name", AMXS_SYNC_DEFAULT);
    }
    if(entry->mib->add_sync_parameters != NULL) {
        ret |= entry->mib->add_sync_parameters(entry->ctx);
    }
    if(ret != amxs_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Mib[%s] failed to create sync context for %s and %s, reason: %d", entry->mib->name, obj_a, obj_b, ret);
        goto exit;
    }

    ret = amxs_sync_ctx_start_sync(entry->ctx);
    if(ret != amxs_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Mib[%s] failed to start sync for %s and %s, reason: %d", entry->mib->name, obj_a, obj_b, ret);
        goto exit;
    }

exit:
    return ret == amxs_status_ok;
}

static const char* ulintf(ctx_htable_entry_t* entry, const char* lower_layer) {
    return entry->is_upper_layer ? entry->netmodel_intf : lower_layer;
}

static const char* llintf(ctx_htable_entry_t* entry, const char* lower_layer) {
    return entry->is_upper_layer ? lower_layer : entry->netmodel_intf;
}

static lower_layer_t* create_lower_layer(const char* lower_layer, ctx_htable_entry_t* entry) {
    lower_layer_t* ll = NULL;

    ll = calloc(1, sizeof(lower_layer_t));
    ll->entry = entry;
    ll->lower_layer = strdup(lower_layer);
    ll->close_query = false;

    amxc_llist_append(&entry->lower_layers, &ll->it);

    return ll;
}

static void lower_layer_clean(amxc_llist_it_t* it) {
    lower_layer_t* ll = amxc_container_of(it, lower_layer_t, it);
    ctx_htable_entry_t* entry = ll->entry;

    if(!string_empty(ll->netmodel_intf)) {
        const char* upper_layer = ulintf(entry, ll->netmodel_intf);
        const char* lower_layer = llintf(entry, ll->netmodel_intf);
        netmodel_unlinkIntfs(upper_layer, lower_layer);

        SAH_TRACEZ_NOTICE(ME, "Unlinked UpperLayer[%s] and LowerLayer[%s]", upper_layer, lower_layer);
    }

    free(ll->lower_layer);
    ll->lower_layer = NULL;

    free(ll->netmodel_intf);
    ll->netmodel_intf = NULL;

    netmodel_closeQuery(ll->query);
    ll->query = NULL;

    free(ll);
}

static char* entry_key(netmodel_mib_t* mib, const char* netmodel_intf) {
    char* key = NULL;
    amxc_string_t key_str;

    amxc_string_init(&key_str, 0);
    amxc_string_setf(&key_str, "%s%s", mib->root, netmodel_intf);
    key = amxc_string_take_buffer(&key_str);
    amxc_string_clean(&key_str);

    return key;
}

static ctx_htable_entry_t* find_entry(netmodel_mib_t* mib, const char* netmodel_intf) {
    amxc_htable_it_t* it = NULL;
    ctx_htable_entry_t* entry = NULL;
    char* key = NULL;

    key = entry_key(mib, netmodel_intf);
    when_str_empty(key, exit);

    it = amxc_htable_get(ctx_htable, key);
    when_null(it, exit);

    entry = amxc_container_of(it, ctx_htable_entry_t, it);

exit:
    if(entry == NULL) {
        SAH_TRACEZ_WARNING(ME, "Mib[%s] entry[%s] not found", mib->name, key != NULL ? key : netmodel_intf);
    }
    free(key);
    return entry;
}

static ctx_htable_entry_t* create_entry(const char* netmodel_intf_object, const char* netmodel_intf_path, netmodel_mib_t* mib) {
    ctx_htable_entry_t* entry = NULL;
    char* path = NULL;
    char* key = NULL;

    when_str_empty(netmodel_intf_object, exit);
    when_str_empty(netmodel_intf_path, exit);
    when_str_empty(mib->root, exit);

    key = entry_key(mib, netmodel_intf_object);
    when_str_empty(key, exit);
    if(amxc_htable_get(ctx_htable, key) != NULL) {
        SAH_TRACEZ_ERROR(ME, "Mib[%s] cannot subscribe a second time on %s", mib->name, netmodel_intf_object);
        goto exit;
    }

    path = strdup(netmodel_intf_path);
    when_null(path, exit);

    entry = calloc(1, sizeof(ctx_htable_entry_t));
    when_null(entry, exit);

    entry->netmodel_intf = path;
    if(string_empty(mib->name)) {
        mib->name = "mib-name-unknown";
    }
    if(string_empty(mib->interfaces_parameter)) {
        mib->interfaces_parameter = "LowerLayers";
    }
    entry->is_upper_layer = flag_is_not_set(mib, NETMODEL_MIB_LINK_AS_LOWER_LAYER);
    entry->mib = mib;

    amxc_llist_init(&entry->lower_layers);

    amxp_timer_new(&entry->timer, retry_timer_cb, entry);
    entry->retry_timeout = MIB_RETRY_TIMEOUT;

    amxc_htable_insert(ctx_htable, key, &entry->it);
exit:
    if(entry == NULL) {
        free(path);
    }
    free(key);
    return entry;
}

static void reset_entry(ctx_htable_entry_t* entry) {
    amxb_subscription_delete(&entry->subscription_interface_path);
    amxb_subscription_delete(&entry->subscription_lower_layers);
    amxb_subscription_delete(&entry->subscription_obj_a);

    amxc_llist_clean(&entry->lower_layers, lower_layer_clean);

    amxs_sync_ctx_stop_sync(entry->ctx);
    amxs_sync_ctx_delete(&entry->ctx);  // careful, some of the code in this file uses ctx->a/b
    SAH_TRACEZ_NOTICE(ME, "Mib[%s] stopped synchronising %s", entry->mib->name, entry->netmodel_intf);

    free(entry->interface_path);
    entry->interface_path = NULL;

    amxp_timer_stop(entry->timer);
    entry->retry_timeout = MIB_RETRY_TIMEOUT;
}

static void entry_clean(ctx_htable_entry_t* entry) {
    when_null(entry, exit);

    reset_entry(entry);

    amxc_htable_it_clean(&entry->it, NULL);

    free(entry->netmodel_intf); // call after reset_entry(), needed to unlink interfaces
    entry->netmodel_intf = NULL;

    amxp_timer_delete(&entry->timer);

    free(entry);
exit:
    return;
}

static void entry_it_clean(UNUSED const char* key, amxc_htable_it_t* it) {
    ctx_htable_entry_t* entry = amxc_container_of(it, ctx_htable_entry_t, it);
    entry_clean(entry);
}

static bool csv_has_string(amxc_llist_t* csv, const char* a) {
    when_str_empty(a, exit);

    amxc_llist_for_each(it, csv) {
        amxc_string_t* string_b = amxc_string_from_llist_it(it);
        const char* b = amxc_string_get(string_b, 0);

        if((b != NULL) && (strcmp(a, b) == 0)) {
            return true;
        }
    }

exit:
    return false;
}

static lower_layer_t* find_lower_layer(ctx_htable_entry_t* entry, const char* lower_layer) {
    amxc_llist_for_each(it, &entry->lower_layers) {
        lower_layer_t* ll = amxc_container_of(it, lower_layer_t, it);
        if((ll->lower_layer != NULL) && (strcmp(ll->lower_layer, lower_layer) == 0)) {
            return ll;
        }
    }

    return NULL;
}

static void resolve_path_handler(UNUSED const char* sig_name, UNUSED const amxc_var_t* data, UNUSED void* priv) {
    lower_layer_t* ll = (lower_layer_t*) priv;
    ctx_htable_entry_t* entry = ll->entry;
    const char* netmodel_intf = NULL;
    const char* upper_layer = NULL;
    const char* lower_layer = NULL;

    when_true((ll->netmodel_intf != NULL), exit);
    when_true(amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, data)), exit);

    netmodel_intf = GET_CHAR(data, "0");
    SAH_TRACEZ_INFO(ME, "Query for %sLayer[%s] returned %s", entry->is_upper_layer ? "Lower" : "Upper", ll->lower_layer, netmodel_intf);
    when_str_empty(netmodel_intf, exit);

    upper_layer = ulintf(entry, netmodel_intf);
    lower_layer = llintf(entry, netmodel_intf);

    netmodel_linkIntfs(upper_layer, lower_layer);
    ll->netmodel_intf = strdup(netmodel_intf);

    if(ll->query != NULL) {
        netmodel_closeQuery(ll->query);
        ll->query = NULL;
    } else {
        ll->close_query = true;
    }

    SAH_TRACEZ_NOTICE(ME, "Linked UpperLayer[%s] and LowerLayer[%s]", upper_layer, lower_layer);
exit:
    return;
}

static void unlink_if_lower_layer_is_removed(amxc_llist_t* to, const char* lower_layer, ctx_htable_entry_t* entry) {
    if(!csv_has_string(to, lower_layer)) {
        lower_layer_t* ll = NULL;

        ll = find_lower_layer(entry, lower_layer);
        if(ll != NULL) {
            amxc_llist_it_take(&ll->it);
            lower_layer_clean(&ll->it); // clean up code of the lower layer should call netmodel_unlinkIntfs
        } else {
            SAH_TRACEZ_WARNING(ME, "Nothing to unlink between UpperLayer[%s] and LowerLayer[%s]?", ulintf(entry, lower_layer), llintf(entry, lower_layer));
        }
    }
}

static void link_if_lower_layer_is_added(amxc_llist_t* from, const char* lower_layer, ctx_htable_entry_t* entry) {
    if(!csv_has_string(from, lower_layer)) {
        lower_layer_t* ll = NULL;

        ll = find_lower_layer(entry, lower_layer);
        if(ll == NULL) {
            const char* subscriber = entry->mib->name;
            amxc_var_t args;
            amxc_var_init(&args);
            amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(cstring_t, &args, "path", lower_layer);
            amxc_var_add_key(cstring_t, &args, "traverse", netmodel_traverse_down);

            ll = create_lower_layer(lower_layer, entry);

            // Open query just in case the NetModel.Intf instance does not yet exist.
            ll->query = netmodel_openQuery("NetModel.Intf.resolver.", subscriber, "resolvePath", &args, resolve_path_handler, ll);

            // If the query result is already ok, close the query here
            if(ll->close_query) {
                netmodel_closeQuery(ll->query);
                ll->query = NULL;
                ll->close_query = false;
            }

            amxc_var_clean(&args);
        } else {
            SAH_TRACEZ_WARNING(ME, "Already linked UpperLayer[%s] and LowerLayer[%s]?", ulintf(entry, lower_layer), llintf(entry, lower_layer));
        }
    }
}

static void lower_layers_handler(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 void* const priv) {
    ctx_htable_entry_t* entry = (ctx_htable_entry_t*) priv;
    amxc_llist_t from;
    amxc_llist_t to;
    amxc_string_t str_from;
    amxc_string_t str_to;
    amxc_var_t* var = NULL;

    SAH_TRACEZ_INFO(ME, "%s%s update", entry->ctx->a, entry->mib->interfaces_parameter);

    amxc_llist_init(&from);
    amxc_llist_init(&to);
    amxc_string_init(&str_from, 0);
    amxc_string_init(&str_to, 0);

    var = amxc_var_get_pathf(data, AMXC_VAR_FLAG_DEFAULT, "parameters.%s", entry->mib->interfaces_parameter);
    amxc_string_set(&str_from, GETP_CHAR(var, "from"));
    amxc_string_split_to_llist(&str_from, &from, ',');
    amxc_string_set(&str_to, GETP_CHAR(var, "to"));
    amxc_string_split_to_llist(&str_to, &to, ',');

    amxc_llist_for_each(it, &from) {
        const char* lower_layer = amxc_string_get(amxc_string_from_llist_it(it), 0);
        unlink_if_lower_layer_is_removed(&to, lower_layer, entry);
    }

    amxc_llist_for_each(it, &to) {
        const char* lower_layer = amxc_string_get(amxc_string_from_llist_it(it), 0);
        link_if_lower_layer_is_added(&from, lower_layer, entry);
    }

    amxc_string_clean(&str_from);
    amxc_string_clean(&str_to);
    amxc_llist_clean(&from, amxc_string_list_it_free);
    amxc_llist_clean(&to, amxc_string_list_it_free);
}

static void link_netmodel_interfaces(ctx_htable_entry_t* entry) {
    amxc_var_t data;
    amxc_var_t result;
    amxc_var_t* var = NULL;
    amxc_string_t str_search_path;
    amxc_string_t expr;
    const char* search_path;
    const char* plugin_instance = entry->ctx->a;
    const char* lower_layers;

    amxc_string_init(&str_search_path, 0);
    amxc_string_init(&expr, 0);
    amxc_var_init(&data);
    amxc_var_init(&result);

    amxc_string_setf(&str_search_path, "%s%s", plugin_instance, entry->mib->interfaces_parameter);
    amxc_string_setf(&expr, "notification == 'dm:object-changed' && contains('parameters.%s')", entry->mib->interfaces_parameter);
    search_path = amxc_string_get(&str_search_path, 0);

    if(AMXB_STATUS_OK != amxb_subscription_new(&entry->subscription_lower_layers, entry->plugin_bus, plugin_instance, amxc_string_get(&expr, 0), lower_layers_handler, entry)) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s", plugin_instance);
        goto exit;
    }

    if(AMXB_STATUS_OK != amxb_get(entry->plugin_bus, search_path, 0, &result, 5)) {
        SAH_TRACEZ_ERROR(ME, "Failed to get parameter %s", search_path);
        goto exit;
    }

    lower_layers = GETP_CHAR(&result, "0.0.0");
    when_str_empty(lower_layers, exit);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    var = amxc_var_add_new_key(&data, "parameters");
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    var = amxc_var_add_new_key(var, entry->mib->interfaces_parameter);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, var, "from", "");
    amxc_var_add_key(cstring_t, var, "to", lower_layers);

    lower_layers_handler(NULL, &data, entry);

exit:
    amxc_string_clean(&str_search_path);
    amxc_string_clean(&expr);
    amxc_var_clean(&data);
    amxc_var_clean(&result);
}

static const char* skip_prefix(const char* path) {
    const char* prefix = "Device.";

    if(strstr(path, prefix) == &path[0]) {
        path = &path[strlen(prefix)];
    }

    return path;
}

static void object_a_changed(UNUSED const char* const sig_name,
                             const amxc_var_t* const data,
                             void* const priv) {
    ctx_htable_entry_t* entry = (ctx_htable_entry_t*) priv;
    amxc_string_t obj_a_str;
    const char* obj_a = NULL;
    char* obj_b = NULL;

    amxc_string_init(&obj_a_str, 0);

    SAH_TRACEZ_INFO(ME, "Mib[%s] received a new obj A", entry->mib->name);
    amxc_string_setf(&obj_a_str, "%s%d.", GET_CHAR(data, "path"), GET_INT32(data, "index"));
    obj_a = amxc_string_get(&obj_a_str, 0);
    when_str_empty_trace(obj_a, exit, ERROR, "Got empty string as obj A");

    if(strcmp(GET_CHAR(data, "notification"), "dm:instance-added") == 0) {
        if(entry->mib->find_object_b_to_sync != NULL) {
            // Some mibs add an extra object to NetModel.Intf for the parameters to sync.
            // For example iprouter sync parameters will be at NetModel.Intf.i.IPv6Router.
            obj_b = entry->mib->find_object_b_to_sync(entry->netmodel_intf);
        } else {
            obj_b = strdup(entry->netmodel_intf);
        }
        when_str_empty_trace(obj_b, exit, ERROR, "Could not find obj B");

        amxs_sync_ctx_stop_sync(entry->ctx);
        amxs_sync_ctx_delete(&entry->ctx); // careful, some of the code in this file uses ctx->a/b
        SAH_TRACEZ_NOTICE(ME, "Mib[%s] stopped synchronising %s", entry->mib->name, entry->netmodel_intf);

        make_sync_context(entry, skip_prefix(obj_a), obj_b);
    } else if(strcmp(GET_CHAR(data, "notification"), "dm:instance-removed") == 0) {
        if((entry->mib->reset_sync_parameters != NULL) && (entry->ctx != NULL)) {
            entry->mib->reset_sync_parameters(entry->ctx);
        }
    }

    // It is possible that the retry timer was started because obj A was not found initially
    // Stop the timer here so we do not restart the sync
    amxp_timer_stop(entry->timer);

exit:
    amxc_string_clean(&obj_a_str);
    free(obj_b);
}

static void start_sync(UNUSED const char* const sig_name,
                       const amxc_var_t* const data,
                       void* const priv) {
    ctx_htable_entry_t* entry = (ctx_htable_entry_t*) priv;
    const char* interface_path = NULL;
    const char* obj_a = NULL;
    const char* obj_b = NULL;
    char* free_me = NULL;
    char* free_me2 = NULL;

    SAH_TRACEZ_INFO(ME, "Mib[%s] Start sync for %s", entry->mib->name, entry->netmodel_intf);

    if(entry->ctx != NULL) { // e.g. in case of NetModel.Intf.{i}.InterfacePath update
        reset_entry(entry);
    }

    interface_path = GETP_CHAR(data, "parameters.InterfacePath.to");
    entry->interface_path = strdup(interface_path);

    entry->plugin_bus = amxb_be_who_has(entry->mib->root);
    when_null_trace(entry->plugin_bus, retry, ERROR,
                    "Mib[%s] couldn't find a bus for root[%s]",
                    entry->mib->name, entry->mib->root);

    // If subscriptions are supported, start one first
    if(entry->mib->create_subscription_for_object_a != NULL) {
        amxc_string_t object_str;
        amxc_string_t expression_str;
        const char* object = NULL;
        const char* expression = NULL;
        amxc_string_init(&object_str, 0);
        amxc_string_init(&expression_str, 256);
        amxc_string_set(&expression_str, "notification in ['dm:instance-added'");
        if(entry->mib->reset_sync_parameters != NULL) {
            amxc_string_appendf(&expression_str, ",'dm:instance-removed'");
        }
        amxc_string_appendf(&expression_str, "]");

        entry->mib->create_subscription_for_object_a(entry->plugin_bus, interface_path, &object_str, &expression_str);
        object = amxc_string_get(&object_str, 0);
        expression = amxc_string_get(&expression_str, 0);
        if(AMXB_STATUS_OK != amxb_subscription_new(&entry->subscription_obj_a, entry->plugin_bus, object, expression, object_a_changed, entry)) {
            SAH_TRACEZ_WARNING(ME, "Mib[%s] failed subscr: O[%s] E[%s]", entry->mib->name, object, expression);
        } else {
            SAH_TRACEZ_INFO(ME, "Mib[%s] created subscr: O[%s] E[%s]", entry->mib->name, object, expression);
        }

        amxc_string_clean(&object_str);
        amxc_string_clean(&expression_str);
    }

    if(entry->mib->find_instance_to_sync != NULL) {
        // Some mibs don't own a NetModel.Intf instance, instead they use another plugin's NetModel.Intf instance.
        // These mibs should use the InterfacePath parameter to find the plugin instance.
        // For example dhcp will sync with a netmodel intf with InterfacePath == "Device.IP.Interface.2.", find_instance_to_sync() will return DHCPv4.Client.[Interface == 'IP.Interface.2.'].
        obj_a = free_me = entry->mib->find_instance_to_sync(entry->plugin_bus, interface_path);
    } else {
        obj_a = interface_path;
    }
    when_str_empty_trace(obj_a, retry, WARNING,
                         "Mib[%s] %s Sync object A not found, waiting...",
                         entry->mib->name, entry->netmodel_intf);

    if(entry->mib->find_object_b_to_sync != NULL) {
        // Some mibs add an extra object to NetModel.Intf for the parameters to sync.
        // For example iprouter sync parameters will be at NetModel.Intf.i.IPv6Router.
        obj_b = free_me2 = entry->mib->find_object_b_to_sync(entry->netmodel_intf);
    } else {
        obj_b = entry->netmodel_intf;
    }

    when_false(make_sync_context(entry, skip_prefix(obj_a), obj_b), retry);

    if(flag_is_not_set(entry->mib, NETMODEL_MIB_DONT_LINK_INTERFACES)) {
        link_netmodel_interfaces(entry);
    }

    SAH_TRACEZ_NOTICE(ME, "Mib[%s] started synchronising %s and %s", entry->mib->name, interface_path, obj_b);

    free(free_me);
    free(free_me2);
    return;

retry:
    amxp_timer_start(entry->timer, entry->retry_timeout);
    free(free_me);
    free(free_me2);
    return;
}

static void retry_timer_cb(UNUSED amxp_timer_t* timer, void* priv) {
    ctx_htable_entry_t* entry = (ctx_htable_entry_t*) priv;
    amxc_var_t data;
    amxc_var_t var;

    // Prepare data from entry for start_sync
    amxc_var_init(&var);
    amxc_var_set(cstring_t, &var, entry->interface_path);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_set_pathf(&data, &var, AMXC_VAR_FLAG_AUTO_ADD, "parameters.InterfacePath.to");

    // Double the timeout
    entry->retry_timeout *= 2;

    // Free the interface path, start_sync will allocate it again
    free(entry->interface_path);
    entry->interface_path = NULL;

    // It is possible that a subscription has been created for obj_a, delete it
    amxb_subscription_delete(&entry->subscription_obj_a);

    // start sync
    start_sync(NULL, &data, entry);

    amxc_var_clean(&data);
    amxc_var_clean(&var);
}

static char* get_parameter_interface_path(const char* netmodel_interface_path) {
    amxc_var_t data;
    amxc_var_t* parameters = NULL;
    char* interface_path = NULL;

    amxc_var_init(&data);

    when_false((AMXB_STATUS_OK == amxb_get(netmodel_get_amxb_bus(), netmodel_interface_path, 0, &data, 5)), exit);

    parameters = GETP_ARG(&data, "0.0");
    when_null(parameters, exit);

    interface_path = amxc_var_take(cstring_t, GET_ARG(parameters, "InterfacePath"));

exit:
    amxc_var_clean(&data);
    return interface_path;
}

bool netmodel_subscribe(const amxc_var_t* const data, netmodel_mib_t* mib) {
    ctx_htable_entry_t* entry = NULL;
    char* interface_path = NULL;
    bool ret = false;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    when_null(data, exit);
    when_null(mib, exit);

    entry = create_entry(GET_CHAR(data, "object"), GET_CHAR(data, "path"), mib);
    when_null(entry, exit);

    if(AMXB_STATUS_OK != amxb_subscription_new(&entry->subscription_interface_path, netmodel_get_amxb_bus(), entry->netmodel_intf, "contains('parameters.InterfacePath')", start_sync, entry)) {
        SAH_TRACEZ_ERROR(ME, "Failed to create subscription on %sInterfacePath", entry->netmodel_intf);
        goto exit;
    }

    interface_path = get_parameter_interface_path(entry->netmodel_intf);
    if(!string_empty(interface_path)) {
        amxc_var_t data1;
        amxc_var_t* var = NULL;

        amxc_var_init(&data1);
        amxc_var_set_type(&data1, AMXC_VAR_ID_HTABLE);
        var = amxc_var_add_new_key(&data1, "parameters");
        amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
        var = amxc_var_add_new_key(var, "InterfacePath");
        amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, var, "to", interface_path);

        start_sync(NULL, &data1, entry);

        amxc_var_clean(&data1);
    } else {
        SAH_TRACEZ_WARNING(ME, "%sInterfacePath is empty, waiting...", entry->netmodel_intf);
    }

    ret = true;

exit:
    free(interface_path);
    if(!ret) {
        entry_clean(entry);
    }
    return ret;
}

void netmodel_unsubscribe(const amxc_var_t* const data, netmodel_mib_t* mib) {
    ctx_htable_entry_t* entry = NULL;
    const char* netmodel_intf = GET_CHAR(data, "object");

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    when_null(mib, exit);
    when_str_empty(netmodel_intf, exit);

    entry = find_entry(mib, netmodel_intf);
    when_null_trace(entry, exit, WARNING, "Mib[%s] entry[%s] not freed (possible memory leak)", mib->name, netmodel_intf);

    entry_clean(entry);

exit:
    return;
}

PRIVATE bool mib_init(void) {
    SAH_TRACEZ_INFO(ME, "Mib init triggered");

    if(ctx_htable == NULL) {
        amxc_htable_new(&ctx_htable, 0); // allocate on heap ensures it is initialised only once
    }

    return true;
}

PRIVATE void mib_cleanup(void) {
    SAH_TRACEZ_INFO(ME, "Mib clean triggered");

    amxc_htable_delete(&ctx_htable, entry_it_clean);  // free from heap ensures it is cleaned up only once
}
