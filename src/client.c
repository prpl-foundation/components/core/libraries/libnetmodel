/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include "netmodel_local.h"
#include "netmodel_mib.h"
#include <netmodel/client.h>

#define ME "netmodel_client"

static amxb_bus_ctx_t* netmodel_amxb_ctx = NULL;

static void netmodel_read(int fd, void* priv) {
    amxb_bus_ctx_t* bus_ctx = (amxb_bus_ctx_t*) priv;
    int rv = amxb_read(bus_ctx);

    if(rv != 0) {
        const char* root = local_get_root_obj_name();
        SAH_TRACEZ_ERROR(ME, "Failed to read from \"pcb:/var/run/nemo\" (%d)", fd);
        amxp_connection_remove(fd);
        amxb_free(&bus_ctx);

        SAH_TRACEZ_WARNING(ME, "Falling back to amxb_be_who_has to find %s", root);
        netmodel_amxb_ctx = amxb_be_who_has(root);
        when_null_trace(netmodel_amxb_ctx, exit, ERROR,
                        "Could not connect to %s object; are you sure the amx bus is up and %s is running?",
                        root, root);
    }

exit:
    return;
}

bool netmodel_initialize(void) {
    bool result = false;
    const char* root = local_get_root_obj_name();

    when_true_status(netmodel_amxb_ctx != NULL, exit, result = true);

    // 1. check if a bus_ctx is available for nemo/netmodel direct connection
    netmodel_amxb_ctx = amxb_find_uri("pcb:/var/run/nemo");
    if(netmodel_amxb_ctx == NULL) {
        // 2. If not found, try to connect to the netmodel direct socket
        if(amxb_connect(&netmodel_amxb_ctx, "pcb:/var/run/nemo") == 0) {
            amxp_connection_add(amxb_get_fd(netmodel_amxb_ctx),
                                netmodel_read,
                                "pcb:/var/run/nemo",
                                AMXP_CONNECTION_BUS,
                                netmodel_amxb_ctx);
        }
    }

    if(netmodel_amxb_ctx == NULL) {
        // 3. If still not connectected, search a connection that can provide
        //    the root object.
        netmodel_amxb_ctx = amxb_be_who_has(root);
    }

    when_null_trace(netmodel_amxb_ctx, exit, ERROR,
                    "Could not connect to %s object; are you sure the amx bus is up and %s is running?",
                    root, root);

    local_init();
    mib_init();

    result = true;

exit:
    return result;
}

void netmodel_cleanup(void) {
    mib_cleanup(); // must be called before local_cleanup to avoid double closing of queries
    local_cleanup();
    netmodel_amxb_ctx = NULL;
}

amxb_bus_ctx_t* netmodel_get_amxb_bus(void) {
    return netmodel_amxb_ctx;
}

const char* netmodel_get_root_obj_name(void) {
    return local_get_root_obj_name();
}
