/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <assert.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "netmodel_local.h"
#include <netmodel/client.h>

#define ME "netmodel_common"

const char* const netmodel_traverse_this = "this";
const char* const netmodel_traverse_down = "down";
const char* const netmodel_traverse_up = "up";
const char* const netmodel_traverse_down_exclusive = "down exclusive";
const char* const netmodel_traverse_up_exclusive = "up exclusive";
const char* const netmodel_traverse_one_level_down = "one level down";
const char* const netmodel_traverse_one_level_up = "one level up";
const char* const netmodel_traverse_all = "all";

bool netmodel_isUp(const char* const interface,
                   const char* const flag,
                   const char* const traverse) {
    bool ret = false;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    ret = local_call_netmodel_function_bool(intf,
                                            "isUp",
                                            NULL,
                                            "flag", AMXC_VAR_ID_CSTRING, flag,
                                            "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                            NULL);
exit:
    free(intf);
    return ret;
}

bool netmodel_hasFlag(const char* const interface,
                      const char* const flag,
                      const char* const condition,
                      const char* const traverse) {
    bool ret = false;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    ret = local_call_netmodel_function_bool(intf,
                                            "hasFlag",
                                            NULL,
                                            "flag", AMXC_VAR_ID_CSTRING, flag,
                                            "condition", AMXC_VAR_ID_CSTRING, condition,
                                            "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                            NULL);
exit:
    free(intf);
    return ret;
}

void netmodel_setFlag(const char* const interface,
                      const char* const flag,
                      const char* const condition,
                      const char* const traverse) {
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    local_call_netmodel_function_void(intf,
                                      "setFlag",
                                      NULL,
                                      "flag", AMXC_VAR_ID_CSTRING, flag,
                                      "condition", AMXC_VAR_ID_CSTRING, condition,
                                      "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                      NULL);
exit:
    free(intf);
}

void netmodel_clearFlag(const char* const interface,
                        const char* const flag,
                        const char* const condition,
                        const char* const traverse) {
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    local_call_netmodel_function_void(intf,
                                      "clearFlag",
                                      NULL,
                                      "flag", AMXC_VAR_ID_CSTRING, flag,
                                      "condition", AMXC_VAR_ID_CSTRING, condition,
                                      "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                      NULL);
exit:
    free(intf);
}

bool netmodel_isLinkedTo(const char* const interface,
                         const char* const target,
                         const char* const traverse) {
    bool ret = false;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    when_str_empty_trace(target, exit, ERROR, "Mandatory argument target is NULL or empty");

    ret = local_call_netmodel_function_bool(intf,
                                            "isLinkedTo",
                                            NULL,
                                            "target", AMXC_VAR_ID_CSTRING, target,
                                            "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                            NULL);
exit:
    free(intf);
    return ret;
}

amxc_var_t* netmodel_getIntfs(const char* const interface,
                              const char* const flag,
                              const char* const traverse) {
    amxc_var_t* ret = NULL;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    ret = local_call_netmodel_function_amxc_var_t(intf,
                                                  "getIntfs",
                                                  NULL,
                                                  "flag", AMXC_VAR_ID_CSTRING, flag,
                                                  "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                                  NULL);
exit:
    free(intf);
    return ret;
}

char* netmodel_luckyIntf(const char* const interface,
                         const char* const flag,
                         const char* const traverse) {
    char* ret = NULL;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    ret = local_call_netmodel_function_cstring(intf,
                                               "luckyIntf",
                                               NULL,
                                               "flag", AMXC_VAR_ID_CSTRING, flag,
                                               "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                               NULL);
exit:
    free(intf);
    return ret;
}

amxc_var_t* netmodel_getParameters(const char* const interface,
                                   const char* const name,
                                   const char* const flag,
                                   const char* const traverse) {
    amxc_var_t* ret = NULL;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    when_str_empty_trace(name, exit, ERROR, "Mandatory argument name is NULL or empty");

    ret = local_call_netmodel_function_amxc_var_t(intf,
                                                  "getParameters",
                                                  NULL,
                                                  "name", AMXC_VAR_ID_CSTRING, name,
                                                  "flag", AMXC_VAR_ID_CSTRING, flag,
                                                  "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                                  NULL);
exit:
    free(intf);
    return ret;
}

amxc_var_t* netmodel_getFirstParameter(const char* const interface,
                                       const char* const name,
                                       const char* const flag,
                                       const char* const traverse) {
    amxc_var_t* ret = NULL;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    when_str_empty_trace(name, exit, ERROR, "Mandatory argument name is NULL or empty");

    ret = local_call_netmodel_function_amxc_var_t(intf,
                                                  "getFirstParameter",
                                                  NULL,
                                                  "name", AMXC_VAR_ID_CSTRING, name,
                                                  "flag", AMXC_VAR_ID_CSTRING, flag,
                                                  "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                                  NULL);
exit:
    free(intf);
    return ret;
}

void netmodel_setParameters(const char* const interface,
                            const char* const name,
                            const amxc_var_t* const value,
                            const char* const flag,
                            const char* const traverse) {
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    when_str_empty_trace(name, exit, ERROR, "Mandatory argument name is NULL or empty");
    when_null_trace(value, exit, ERROR, "Mandatory argument value is NULL");

    local_call_netmodel_function_void(intf,
                                      "setParameters",
                                      NULL,
                                      "name", AMXC_VAR_ID_CSTRING, name,
                                      "value", AMXC_VAR_ID_ANY, value,
                                      "flag", AMXC_VAR_ID_CSTRING, flag,
                                      "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                      NULL);
exit:
    free(intf);
}

void netmodel_setFirstParameter(const char* const interface,
                                const char* const name,
                                const amxc_var_t* const value,
                                const char* const flag,
                                const char* const traverse) {
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    when_str_empty_trace(name, exit, ERROR, "Mandatory argument name is NULL or empty");
    when_null_trace(value, exit, ERROR, "Mandatory argument value is NULL");

    local_call_netmodel_function_void(intf,
                                      "setFirstParameter",
                                      NULL,
                                      "name", AMXC_VAR_ID_CSTRING, name,
                                      "value", AMXC_VAR_ID_ANY, value,
                                      "flag", AMXC_VAR_ID_CSTRING, flag,
                                      "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                      NULL);
exit:
    free(intf);
}

amxc_var_t* netmodel_getAddrs(const char* const interface,
                              const char* const flag,
                              const char* const traverse) {
    amxc_var_t* ret = NULL;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    ret = local_call_netmodel_function_amxc_var_t(intf,
                                                  "getAddrs",
                                                  NULL,
                                                  "flag", AMXC_VAR_ID_CSTRING, flag,
                                                  "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                                  NULL);
exit:
    free(intf);
    return ret;
}

amxc_var_t* netmodel_luckyAddr(const char* const interface,
                               const char* const flag,
                               const char* const traverse) {
    amxc_var_t* ret = NULL;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    ret = local_call_netmodel_function_amxc_var_t(intf,
                                                  "luckyAddr",
                                                  NULL,
                                                  "flag", AMXC_VAR_ID_CSTRING, flag,
                                                  "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                                  NULL);
exit:
    free(intf);
    return ret;
}

char* netmodel_luckyAddrAddress(const char* const interface,
                                const char* const flag,
                                const char* const traverse) {
    char* ret = NULL;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    ret = local_call_netmodel_function_cstring(intf,
                                               "luckyAddrAddress",
                                               NULL,
                                               "flag", AMXC_VAR_ID_CSTRING, flag,
                                               "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                               NULL);
exit:
    free(intf);
    return ret;
}

amxc_var_t* netmodel_getDHCPOption(const char* const interface,
                                   const char* const type,
                                   uint16_t tag,
                                   const char* const traverse) {
    amxc_var_t* ret = NULL;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    when_str_empty_trace(type, exit, ERROR, "Mandatory argument type is NULL or empty");

    ret = local_call_netmodel_function_amxc_var_t(intf,
                                                  "getDHCPOption",
                                                  NULL,
                                                  "type", AMXC_VAR_ID_CSTRING, type,
                                                  "tag", AMXC_VAR_ID_UINT16, tag,
                                                  "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                                  NULL);
exit:
    free(intf);
    return ret;
}

amxc_var_t* netmodel_getMibs(const char* const interface,
                             const char* const flag,
                             const char* const traverse) {
    amxc_var_t* ret = NULL;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    ret = local_call_netmodel_function_amxc_var_t(intf,
                                                  "getMibs",
                                                  NULL,
                                                  "flag", AMXC_VAR_ID_CSTRING, flag,
                                                  "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                                  NULL);
exit:
    free(intf);
    return ret;
}

amxc_var_t* netmodel_getIPv6Prefixes(const char* const interface,
                                     const char* const flag,
                                     const char* const traverse) {
    amxc_var_t* ret = NULL;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    ret = local_call_netmodel_function_amxc_var_t(intf,
                                                  "getIPv6Prefixes",
                                                  NULL,
                                                  "flag", AMXC_VAR_ID_CSTRING, flag,
                                                  "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                                  NULL);
exit:
    free(intf);
    return ret;
}

amxc_var_t* netmodel_getFirstIPv6Prefix(const char* const interface,
                                        const char* const flag,
                                        const char* const traverse) {
    amxc_var_t* ret = NULL;
    char* intf = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    intf = local_resolve_first_intf(interface);
    when_null(intf, exit);

    ret = local_call_netmodel_function_amxc_var_t(intf,
                                                  "getFirstIPv6Prefix",
                                                  NULL,
                                                  "flag", AMXC_VAR_ID_CSTRING, flag,
                                                  "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                                  NULL);
exit:
    free(intf);
    return ret;
}

void netmodel_linkIntfs(const char* const ulintf, const char* const llintf) {
    char* ul = NULL;
    char* ll = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    when_str_empty_trace(ulintf, exit, ERROR, "Mandatory argument ulintf is NULL or empty");
    when_str_empty_trace(llintf, exit, ERROR, "Mandatory argument llintf is NULL or empty");

    ul = local_resolve_first_intf(ulintf);
    when_null(ul, exit);
    ll = local_resolve_first_intf(llintf);
    when_null(ll, exit);

    local_call_netmodel_function_void(local_get_root_obj_name(),
                                      "linkIntfs",
                                      NULL,
                                      "ulintf", AMXC_VAR_ID_CSTRING, ul,
                                      "llintf", AMXC_VAR_ID_CSTRING, ll,
                                      NULL);
exit:
    free(ul);
    free(ll);
}

void netmodel_unlinkIntfs(const char* const ulintf, const char* const llintf) {
    char* ul = NULL;
    char* ll = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    when_str_empty_trace(ulintf, exit, ERROR, "Mandatory argument ulintf is NULL or empty");
    when_str_empty_trace(llintf, exit, ERROR, "Mandatory argument llintf is NULL or empty");

    ul = local_resolve_first_intf(ulintf);
    when_null(ul, exit);
    ll = local_resolve_first_intf(llintf);
    when_null(ll, exit);

    local_call_netmodel_function_void(local_get_root_obj_name(),
                                      "unlinkIntfs",
                                      NULL,
                                      "ulintf", AMXC_VAR_ID_CSTRING, ul,
                                      "llintf", AMXC_VAR_ID_CSTRING, ll,
                                      NULL);
exit:
    free(ul);
    free(ll);
}

void netmodel_closeQuery(netmodel_query_t* const netmodel_query) {
    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    local_async_close_query(netmodel_query);

exit:
    return;
}

netmodel_query_t* netmodel_openQuery(const char* const interface,
                                     const char* const subscriber,
                                     const char* const cl,
                                     const amxc_var_t* const arguments,
                                     netmodel_callback_t handler,
                                     void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    ret = local_async_open_query(interface,
                                 subscriber,
                                 cl,
                                 handler,
                                 userdata,
                                 arguments,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_isUp(const char* const interface,
                                          const char* const subscriber,
                                          const char* const flag,
                                          const char* const traverse,
                                          netmodel_callback_t handler,
                                          void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    ret = local_async_open_query(interface,
                                 subscriber,
                                 "isUp",
                                 handler,
                                 userdata,
                                 NULL,
                                 "flag", AMXC_VAR_ID_CSTRING, flag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_hasFlag(const char* const interface,
                                             const char* const subscriber,
                                             const char* const flag,
                                             const char* const condition,
                                             const char* const traverse,
                                             netmodel_callback_t handler,
                                             void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    ret = local_async_open_query(interface,
                                 subscriber,
                                 "hasFlag",
                                 handler,
                                 userdata,
                                 NULL,
                                 "condition", AMXC_VAR_ID_CSTRING, condition,
                                 "flag", AMXC_VAR_ID_CSTRING, flag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_isLinkedTo(const char* const interface,
                                                const char* const subscriber,
                                                const char* const target,
                                                const char* const traverse,
                                                netmodel_callback_t handler,
                                                void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    when_str_empty_trace(target, exit, ERROR, "Mandatory argument target is NULL or empty");

    ret = local_async_open_query(interface,
                                 subscriber,
                                 "isLinkedTo",
                                 handler,
                                 userdata,
                                 NULL,
                                 "target", AMXC_VAR_ID_CSTRING, target,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_getIntfs(const char* const interface,
                                              const char* const subscriber,
                                              const char* const flag,
                                              const char* const traverse,
                                              netmodel_callback_t handler,
                                              void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    ret = local_async_open_query(interface,
                                 subscriber,
                                 "getIntfs",
                                 handler,
                                 userdata,
                                 NULL,
                                 "flag", AMXC_VAR_ID_CSTRING, flag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_luckyIntf(const char* const interface,
                                               const char* const subscriber,
                                               const char* const flag,
                                               const char* const traverse,
                                               netmodel_callback_t handler,
                                               void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    ret = local_async_open_query(interface,
                                 subscriber,
                                 "luckyIntf",
                                 handler,
                                 userdata,
                                 NULL,
                                 "flag", AMXC_VAR_ID_CSTRING, flag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_getParameters(const char* const interface,
                                                   const char* const subscriber,
                                                   const char* const name,
                                                   const char* const flag,
                                                   const char* const traverse,
                                                   netmodel_callback_t handler,
                                                   void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    when_str_empty_trace(name, exit, ERROR, "Mandatory argument name is NULL or empty");

    ret = local_async_open_query(interface,
                                 subscriber,
                                 "getParameters",
                                 handler,
                                 userdata,
                                 NULL,
                                 "name", AMXC_VAR_ID_CSTRING, name,
                                 "flag", AMXC_VAR_ID_CSTRING, flag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_getFirstParameter(const char* const interface,
                                                       const char* const subscriber,
                                                       const char* const name,
                                                       const char* const flag,
                                                       const char* const traverse,
                                                       netmodel_callback_t handler,
                                                       void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    when_str_empty_trace(name, exit, ERROR, "Mandatory argument name is NULL or empty");

    ret = local_async_open_query(interface,
                                 subscriber,
                                 "getFirstParameter",
                                 handler,
                                 userdata,
                                 NULL,
                                 "name", AMXC_VAR_ID_CSTRING, name,
                                 "flag", AMXC_VAR_ID_CSTRING, flag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_getAddrs(const char* const interface,
                                              const char* const subscriber,
                                              const char* const flag,
                                              const char* const traverse,
                                              netmodel_callback_t handler,
                                              void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    ret = local_async_open_query(interface,
                                 subscriber,
                                 "getAddrs",
                                 handler,
                                 userdata,
                                 NULL,
                                 "flag", AMXC_VAR_ID_CSTRING, flag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_luckyAddr(const char* const interface,
                                               const char* const subscriber,
                                               const char* const flag,
                                               const char* const traverse,
                                               netmodel_callback_t handler,
                                               void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    ret = local_async_open_query(interface,
                                 subscriber,
                                 "luckyAddr",
                                 handler,
                                 userdata,
                                 NULL,
                                 "flag", AMXC_VAR_ID_CSTRING, flag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_luckyAddrAddress(const char* const interface,
                                                      const char* const subscriber,
                                                      const char* const flag,
                                                      const char* const traverse,
                                                      netmodel_callback_t handler,
                                                      void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    ret = local_async_open_query(interface,
                                 subscriber,
                                 "luckyAddrAddress",
                                 handler,
                                 userdata,
                                 NULL,
                                 "flag", AMXC_VAR_ID_CSTRING, flag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_getDHCPOption(const char* const interface,
                                                   const char* const subscriber,
                                                   const char* const type,
                                                   uint16_t tag,
                                                   const char* const traverse,
                                                   netmodel_callback_t handler,
                                                   void* userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    when_str_empty_trace(type, exit, ERROR, "Mandatory argument type is NULL or empty");

    ret = local_async_open_query(interface,
                                 subscriber,
                                 "getDHCPOption",
                                 handler,
                                 userdata,
                                 NULL,
                                 "type", AMXC_VAR_ID_CSTRING, type,
                                 "tag", AMXC_VAR_ID_UINT16, tag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_getIPv6Prefixes(const char* const interface,
                                                     const char* const subscriber,
                                                     const char* const flag,
                                                     const char* const traverse,
                                                     netmodel_callback_t handler,
                                                     void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    ret = local_async_open_query(interface,
                                 subscriber,
                                 "getIPv6Prefixes",
                                 handler,
                                 userdata,
                                 NULL,
                                 "flag", AMXC_VAR_ID_CSTRING, flag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}

netmodel_query_t* netmodel_openQuery_getFirstIPv6Prefix(const char* const interface,
                                                        const char* const subscriber,
                                                        const char* const flag,
                                                        const char* const traverse,
                                                        netmodel_callback_t handler,
                                                        void* const userdata) {
    netmodel_query_t* ret = NULL;

    when_null_trace(netmodel_get_amxb_bus(), exit, ERROR, "NetModel library is not initialized");
    ret = local_async_open_query(interface,
                                 subscriber,
                                 "getFirstIPv6Prefix",
                                 handler,
                                 userdata,
                                 NULL,
                                 "flag", AMXC_VAR_ID_CSTRING, flag,
                                 "traverse", AMXC_VAR_ID_CSTRING, traverse,
                                 NULL);
exit:
    return ret;
}
