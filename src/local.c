/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <assert.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "netmodel_local.h"
#include <netmodel/client.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_path.h>

#define ME "netmodel_local"

#ifndef ROOT_OBJ_NAME
#define ROOT_OBJ_NAME "NetModel"
#endif

static amxc_llist_t* query_list = NULL;
static bool query_list_locked = false;
static const char* root_obj_name = ROOT_OBJ_NAME;

static void local_netmodel_query_unsubscribe(netmodel_query_impl_t* const impl);
static void local_netmodel_query_list_it_close(amxc_llist_it_t* const it);
static netmodel_query_t* local_netmodel_query_create(const char* const given_intf,
                                                     const char* const interface,
                                                     const char* const subscriber,
                                                     const char* const cl,
                                                     amxc_var_t* const arguments,
                                                     const uint32_t query_id,
                                                     netmodel_callback_t handler,
                                                     void* const userdata);
static netmodel_query_t* local_netmodel_query_new(const char* const given_intf,
                                                  const char* const interface,
                                                  const char* const subscriber,
                                                  const char* const cl,
                                                  amxc_var_t* const arguments,
                                                  const uint32_t query_id,
                                                  netmodel_callback_t handler,
                                                  void* const userdata);
static netmodel_query_t* local_netmodel_query_new_existing_impl(netmodel_query_impl_t* impl,
                                                                netmodel_callback_t handler,
                                                                void* const userdata);
static void local_resolve_handler(const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv);
static void local_reply_handler(const char* const sig_name,
                                const amxc_var_t* const data,
                                void* const priv);
static void local_removal_handler(const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv);

static int local_copy_valist_into_variant(amxc_var_t* const variant_args, va_list variadic_list) {
    const char* argument_name = NULL;
    uint32_t argument_type = AMXC_VAR_ID_ANY;

    if((NULL == variant_args) || (amxc_var_type_of(variant_args) != AMXC_VAR_ID_HTABLE)) {
        SAH_TRACEZ_WARNING(ME, "Invalid input arguments");
        goto exit;
    }

    while(NULL != (argument_name = va_arg(variadic_list, const char*))) {
        argument_type = va_arg(variadic_list, uint32_t);
        switch(argument_type) {
        case AMXC_VAR_ID_CSTRING: {
            const char* const string_value = va_arg(variadic_list, const char*);
            if(NULL != string_value) {
                amxc_var_t* const variant = amxc_var_add_key(cstring_t, variant_args, argument_name, string_value);
                when_null(variant, exit);
            }
            break;
        }
        case AMXC_VAR_ID_UINT16: {
            uint16_t value = va_arg(variadic_list, unsigned int);
            amxc_var_t* const variant = amxc_var_add_key(uint16_t, variant_args, argument_name, value);
            when_null(variant, exit);
            break;
        }
        case AMXC_VAR_ID_ANY: {
            amxc_var_t* const variant_value = va_arg(variadic_list, amxc_var_t*);
            amxc_var_t* const variant_element = amxc_var_add_new_key(variant_args, argument_name);
            when_null(variant_element, exit);
            amxc_var_move(variant_element, variant_value);
            break;
        }
        default:
            SAH_TRACEZ_INFO(ME, "Unsupported argument type=[%u]", argument_type);
            goto exit;
        }
    }
    return 0;
exit:
    return -1;
}

static void local_netmodel_query_cb_delete(netmodel_query_cb_t* cb) {
    when_null_trace(cb, exit, WARNING, "Query callback info is NULL");

    amxc_llist_it_take(&cb->it);
    free(cb);

exit:
    return;
}

static void local_netmodel_query_cb_list_it_delete(amxc_llist_it_t* const it) {
    netmodel_query_cb_t* impl = amxc_container_of(it, netmodel_query_cb_t, it);

    local_netmodel_query_cb_delete(impl);
}

static void local_netmodel_query_cb_close(netmodel_query_cb_t* cb) {
    when_null_trace(cb, exit, WARNING, "Query callback info is NULL");

    cb->ref_count--;
    when_false(cb->ref_count <= 0, exit);

    if(query_list_locked) {
        cb->should_destroy = true;
    } else {
        local_netmodel_query_cb_delete(cb);
    }

exit:
    return;
}

static netmodel_query_cb_t* local_netmodel_query_cb_find(const netmodel_query_impl_t* impl,
                                                         netmodel_callback_t handler,
                                                         void* const userdata) {
    netmodel_query_cb_t* cb = NULL;
    when_null(impl, exit);

    amxc_llist_iterate(it, &impl->cbs) {
        cb = amxc_llist_it_get_data(it, netmodel_query_cb_t, it);
        when_true((cb->handler == handler) && (cb->userdata == userdata), exit);
        cb = NULL;
    }

exit:
    return cb;
}

static netmodel_query_cb_t* local_netmodel_query_cb_new(netmodel_callback_t handler,
                                                        void* const userdata) {
    netmodel_query_cb_t* cb = (netmodel_query_cb_t*) calloc(1, sizeof(netmodel_query_cb_t));
    when_null_trace(cb, exit, ERROR, "Failed to allocate netmodel_query_cb_t");

    amxc_llist_it_init(&cb->it);
    cb->handler = handler;
    cb->userdata = userdata;
    cb->ref_count = 1;
    cb->should_destroy = false;

exit:
    return cb;
}

static void local_netmodel_query_impl_delete(netmodel_query_impl_t* impl) {
    when_null_trace(impl, exit, WARNING, "Query implementation info is NULL");

    SAH_TRACEZ_INFO(ME, "Destroying query on object=[%s]", impl->object);
    amxc_var_clean(&impl->arguments);
    amxc_var_clean(&impl->last_result);
    amxc_llist_it_take(&impl->it);
    free(impl->object);
    free(impl->given_intf);
    free(impl->interface);
    free(impl->cl);
    free(impl->subscriber);
    amxb_subscription_delete(&impl->result);
    amxb_subscription_delete(&impl->removal);
    free(impl);

exit:
    return;
}

static void local_netmodel_query_impl_list_it_delete(amxc_llist_it_t* const it) {
    netmodel_query_impl_t* impl = amxc_container_of(it, netmodel_query_impl_t, it);

    local_netmodel_query_impl_delete(impl);
}

static void local_netmodel_query_impl_close(netmodel_query_impl_t** impl) {
    netmodel_query_impl_t* tmp = NULL;
    when_null_trace(impl, exit, WARNING, "Query implementation info is NULL");
    when_null_trace(*impl, exit, WARNING, "Query implementation info is NULL");

    tmp = *impl;

    tmp->ref_count--;
    when_false(tmp->ref_count <= 0, exit);

    // ID == 0 means this is a placeholder query, used in path resolve scenarios
    // These queries don't have a NetModel counterpart and thus only need to be freed
    if(tmp->query_id != 0) {
        local_netmodel_query_unsubscribe(tmp);
        local_call_netmodel_function_void(tmp->interface,
                                          "closeQuery",
                                          &tmp->arguments,
                                          "subscriber", AMXC_VAR_ID_CSTRING, tmp->subscriber,
                                          "class", AMXC_VAR_ID_CSTRING, tmp->cl,
                                          NULL);
    }

    amxc_llist_clean(&(tmp->queries), local_netmodel_query_list_it_close);
    if(query_list_locked) {
        tmp->should_destroy = true;
    } else {
        local_netmodel_query_impl_delete(tmp);
        *impl = NULL;
    }

exit:
    return;
}

static netmodel_query_impl_t* local_netmodel_query_impl_new(const char* const given_intf,
                                                            const char* const interface,
                                                            const char* const subscriber,
                                                            const char* const cl,
                                                            amxc_var_t* const arguments,
                                                            const uint32_t query_id) {
    netmodel_query_impl_t* impl = NULL;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    impl = (netmodel_query_impl_t*) calloc(1, sizeof(netmodel_query_impl_t));
    when_null_trace(impl, exit, ERROR, "Failed to allocate netmodel_query_impl_t");

    amxc_var_init(&impl->arguments);
    amxc_var_init(&impl->last_result);
    amxc_llist_init(&impl->queries);
    amxc_llist_init(&impl->cbs);
    amxc_llist_it_init(&impl->it);

    impl->given_intf = strdup(given_intf);
    when_null_trace(impl->given_intf, exit, ERROR, "Strdup(%s) failed", given_intf);

    impl->interface = strdup(interface);
    when_null_trace(impl->interface, exit, ERROR, "Strdup(%s) failed", interface);

    impl->subscriber = strdup(subscriber);
    when_null_trace(impl->subscriber, exit, ERROR, "Strdup(%s) failed", subscriber);

    impl->cl = strdup(cl);
    when_null_trace(impl->cl, exit, ERROR, "Strdup(%s) failed", cl);

    when_failed(amxc_var_copy(&impl->arguments, arguments), exit);

    impl->query_id = query_id;
    impl->ref_count = 1;
    impl->should_destroy = false;
    impl->result = NULL;
    impl->removal = NULL;

    amxc_string_setf(&path, "%s.Intf.%s.Query.%u.", root_obj_name, interface, query_id);
    impl->object = amxc_string_take_buffer(&path);
    when_null_trace(impl->object, exit, ERROR, "Strdup(%s) failed", amxc_string_get(&path, 0));

    amxc_string_clean(&path);

    return impl;

exit:
    local_netmodel_query_impl_delete(impl);
    amxc_string_clean(&path);
    return NULL;
}

static bool local_netmodel_query_impl_cmp(const netmodel_query_impl_t* impl,
                                          const char* const interface,
                                          const char* const subscriber,
                                          const char* const cl,
                                          amxc_var_t* args,
                                          bool allow_placeholder) {
    int ret = -1;

    if((strcmp(impl->given_intf, interface) == 0) &&
       (strcmp(impl->subscriber, subscriber) == 0) &&
       (strcmp(impl->cl, cl) == 0) &&
       (amxc_var_compare(&impl->arguments, args, &ret) == 0) &&
       (ret == 0)) {
        if((impl->query_id == 0) || (strcmp(impl->given_intf, "resolver") == 0)) {
            return allow_placeholder;
        } else {
            return true;
        }
    }

    return false;
}

static netmodel_query_impl_t* local_netmodel_query_impl_find_in_list(amxc_llist_t* list,
                                                                     const char* const interface,
                                                                     const char* const subscriber,
                                                                     const char* const cl,
                                                                     amxc_var_t* args,
                                                                     bool allow_placeholder) {
    netmodel_query_impl_t* impl = NULL;
    amxc_llist_iterate(it, list) {
        netmodel_query_t* q = amxc_llist_it_get_data(it, netmodel_query_t, it);
        impl = q->impl;

        if(local_netmodel_query_impl_cmp(impl, interface, subscriber, cl, args, allow_placeholder)) {
            break;
        }

        impl = local_netmodel_query_impl_find_in_list(&impl->queries, interface, subscriber, cl, args, allow_placeholder);
        if(impl != NULL) {
            break;
        }
    }

    return impl;
}

static netmodel_query_impl_t* local_netmodel_query_impl_find(const char* const interface,
                                                             const char* const subscriber,
                                                             const char* const cl,
                                                             amxc_var_t* args,
                                                             bool allow_placeholder) {
    return local_netmodel_query_impl_find_in_list(query_list, interface, subscriber, cl, args, allow_placeholder);
}

static void local_netmodel_query_impl_call_cbs(netmodel_query_impl_t* impl,
                                               const char* const object,
                                               const amxc_var_t* const data) {
    when_null_trace(impl, exit, ERROR, "Query implementation is NULL");
    when_null_trace(data, exit, ERROR, "Data is NULL");
    when_str_empty_trace(object, exit, ERROR, "Object string is empty");

    query_list_locked = true;
    amxc_llist_for_each(it, &impl->cbs) {
        netmodel_query_cb_t* cb = amxc_container_of(it, netmodel_query_cb_t, it);
        if(cb->handler != NULL) {
            cb->handler(object, data, cb->userdata);
        }
    }
    query_list_locked = false;

exit:
    return;
}

static void local_netmodel_query_delete(netmodel_query_t* q) {
    when_null_trace(q, exit, WARNING, "Query is NULL");

    amxc_llist_it_take(&q->it);
    free(q);

exit:
    return;
}

static void local_netmodel_query_check_for_deletion(amxc_llist_t* list) {
    amxc_llist_t impls_to_free;
    amxc_llist_t cbs_to_free;

    amxc_llist_init(&impls_to_free);
    amxc_llist_init(&cbs_to_free);

    amxc_llist_for_each(it, list) {
        netmodel_query_t* q = amxc_llist_it_get_data(it, netmodel_query_t, it);
        if((q->cb != NULL) && q->cb->should_destroy) {
            amxc_llist_append(&cbs_to_free, &q->cb->it);
            q->cb = NULL;
        }

        local_netmodel_query_check_for_deletion(&q->impl->queries);
        if((q->impl != NULL) && (q->impl->should_destroy)) {
            amxc_llist_append(&impls_to_free, &q->impl->it);
            q->impl = NULL;
        }

        if(q->should_destroy) {
            local_netmodel_query_delete(q);
        }
    }

    amxc_llist_clean(&cbs_to_free, local_netmodel_query_cb_list_it_delete);
    amxc_llist_clean(&impls_to_free, local_netmodel_query_impl_list_it_delete);
}

static PRIVATE netmodel_query_t* local_async_open_query_list(const char* const given_intf,
                                                             const amxc_var_t* const intf_list,
                                                             const char* const subscriber,
                                                             const char* const cl,
                                                             netmodel_callback_t handler,
                                                             void* const userdata,
                                                             amxc_var_t* const args) {
    netmodel_query_t* q = NULL;
    amxc_llist_t q_list;
    amxc_var_t args_copy;
    bool cleanup = false;

    amxc_llist_init(&q_list);
    amxc_var_init(&args_copy);

    amxc_var_for_each(var, intf_list) {
        uint32_t id = 0;
        const char* intf = amxc_var_constcast(cstring_t, var);

        netmodel_query_impl_t* impl = local_netmodel_query_impl_find(given_intf, subscriber, cl, args, false);
        if(impl != NULL) {
            q = local_netmodel_query_new_existing_impl(impl, handler, userdata);
            if(q == NULL) {
                SAH_TRACEZ_ERROR(ME, "Failed to initialize netmodel query data");
                cleanup = true;
                goto exit;
            }

            amxc_llist_append(&q_list, &q->it);
            continue;
        }

        amxc_var_copy(&args_copy, args);
        id = local_call_netmodel_function_uint32_t(intf,
                                                   "openQuery",
                                                   &args_copy,
                                                   "subscriber", AMXC_VAR_ID_CSTRING, subscriber,
                                                   "class", AMXC_VAR_ID_CSTRING, cl,
                                                   NULL);
        SAH_TRACEZ_INFO(ME, "Opened query on interface %s, id = %u", intf, id);
        if(0 == id) {
            SAH_TRACEZ_ERROR(ME, "OpenQuery(intf=%s, subscriber=%s, class=%s) failed", intf, subscriber, cl);
            cleanup = true;
            goto exit;
        }

        q = local_netmodel_query_create(given_intf, intf, subscriber, cl, args, id, handler, userdata);
        if(NULL == q) {
            SAH_TRACEZ_ERROR(ME, "Failed to create local query object for query (intf=%s, subscriber=%s, class=%s)", intf, subscriber, cl);
            cleanup = true;
            goto exit;
        }

        amxc_llist_append(&q_list, &q->it);
    }

    q = amxc_container_of(amxc_llist_take_first(&q_list), netmodel_query_t, it);
    amxc_llist_move(&q->impl->queries, &q_list);
    amxc_llist_append(query_list, &q->it);

exit:
    if(cleanup) {
        amxc_llist_clean(&q_list, local_netmodel_query_list_it_close);
    }
    amxc_llist_clean(&q_list, NULL);
    amxc_var_clean(&args_copy);
    return q;
}

static PRIVATE netmodel_query_t* local_async_open_resolve_query(const char* const interface,
                                                                const char* const subscriber,
                                                                const char* const cl,
                                                                netmodel_callback_t handler,
                                                                void* const userdata,
                                                                amxc_var_t* const args) {
    netmodel_query_t* q = NULL;
    netmodel_query_t* rq = NULL;
    amxc_var_t resolve_args;
    amxc_string_t str;
    bool cleanup = false;

    amxc_string_init(&str, 0);
    amxc_var_init(&resolve_args);
    amxc_var_set_type(&resolve_args, AMXC_VAR_ID_HTABLE);

    SAH_TRACEZ_INFO(ME, "Opening resolver query on interface %s", interface);

    q = local_netmodel_query_new(interface, "original_query_data", subscriber, cl, args, 0, handler, userdata);
    when_null(q, exit);

    amxc_var_add_key(cstring_t, &resolve_args, "path", interface);
    rq = local_async_open_query("resolver", subscriber, "resolvePath", local_resolve_handler, q, &resolve_args, NULL);
    if(NULL == rq) {
        cleanup = true;
        SAH_TRACEZ_ERROR(ME, "Failed to open path resolve query for %s", interface);
        goto exit;
    }

    // The resolve handler will be called once already
    // If the path was resolved, q should have at least 1 query in its queries list
    // So if the list is not empty, close the resolve query
    if(!amxc_llist_is_empty(&q->impl->queries)) {
        local_async_close_query(rq);
    } else {
        amxc_llist_append(&q->impl->queries, &rq->it);
        if(NULL != handler) {
            amxc_var_t data;

            amxc_var_init(&data);
            query_list_locked = true;
            handler("Resolver", &data, userdata);
            query_list_locked = false;

            amxc_var_clean(&data);
        }
    }

    amxc_llist_append(query_list, &q->it);

exit:
    amxc_var_clean(&resolve_args);
    if(cleanup) {
        local_async_close_query(q);
        q = NULL;
    }
    return q;
}

static void local_netmodel_query_unsubscribe(netmodel_query_impl_t* const impl) {
    when_null(impl, exit);

    amxb_subscription_delete(&impl->result);
    amxb_subscription_delete(&impl->removal);

exit:
    return;
}

static netmodel_query_t* local_netmodel_query_new(const char* const given_intf,
                                                  const char* const interface,
                                                  const char* const subscriber,
                                                  const char* const cl,
                                                  amxc_var_t* const arguments,
                                                  const uint32_t query_id,
                                                  netmodel_callback_t handler,
                                                  void* const userdata) {
    netmodel_query_t* q = (netmodel_query_t*) calloc(1, sizeof(netmodel_query_t));
    when_null_trace(q, exit, ERROR, "Failed to allocate netmodel_query_t");

    amxc_llist_it_init(&q->it);
    q->should_destroy = false;
    q->impl = local_netmodel_query_impl_new(given_intf, interface, subscriber, cl, arguments, query_id);
    when_null_trace(q->impl, exit, ERROR, "Failed to initialize query implementation data");

    // No need to install callbacks if no handler is given
    if(handler != NULL) {
        q->cb = local_netmodel_query_cb_new(handler, userdata);
        when_null_trace(q->cb, exit, ERROR, "Failed to initialize query callback data");
        amxc_llist_append(&q->impl->cbs, &q->cb->it);
    }

    return q;

exit:
    if(q != NULL) {
        local_netmodel_query_impl_delete(q->impl);
    }
    free(q);
    return NULL;
}

static netmodel_query_t* local_netmodel_query_new_existing_impl(netmodel_query_impl_t* impl,
                                                                netmodel_callback_t handler,
                                                                void* const userdata) {
    amxc_var_t retval;
    netmodel_query_t* q = NULL;
    netmodel_query_impl_t* impl_to_use = NULL;
    bool cleanup = true;

    amxc_var_init(&retval);

    if(impl->query_id == 0) {
        // This is a placeholder
        // Get child
        SAH_TRACEZ_INFO(ME, "Query is a placeholder, trying to find a suitable child query");
        amxc_llist_iterate(it, &impl->queries) {
            netmodel_query_t* child = amxc_llist_it_get_data(it, netmodel_query_t, it);
            if(strcmp("resolver", child->impl->given_intf) == 0) {
                // Child is a resolver
                // Opening a resolver for the same impl will use the resolver that is already there
                q = local_async_open_resolve_query(impl->given_intf, impl->subscriber, impl->cl,
                                                   handler, userdata, &impl->arguments);
                cleanup = false;
                goto exit;
            } else {
                impl_to_use = child->impl;
                SAH_TRACEZ_INFO(ME, "Using child query with id %u", impl_to_use->query_id);
                break;
            }
        }
    } else {
        // This is a normal query
        impl_to_use = impl;
        SAH_TRACEZ_INFO(ME, "Reusing query with id %u", impl_to_use->query_id);
    }

    when_null_trace(impl_to_use, exit, ERROR, "Could not find a suitable child query");

    q = (netmodel_query_t*) calloc(1, sizeof(netmodel_query_t));
    when_null_trace(q, exit, ERROR, "Failed to allocate netmodel_query_t");

    q->impl = impl_to_use;
    q->impl->ref_count++;
    amxc_llist_append(query_list, &q->it);

    if(handler != NULL) {
        netmodel_query_cb_t* cb = local_netmodel_query_cb_find(impl_to_use, handler, userdata);

        if(cb == NULL) {
            cb = local_netmodel_query_cb_new(handler, userdata);
            when_null_trace(cb, exit, ERROR, "Failed to initialize query callback data");
        } else {
            cb->ref_count++;
        }

        q->cb = cb;
        amxc_llist_append(&impl_to_use->cbs, &cb->it);

        query_list_locked = true;
        handler(impl_to_use->object, &impl_to_use->last_result, userdata);
        query_list_locked = false;

        local_netmodel_query_check_for_deletion(query_list);
    }

    cleanup = false;

exit:
    if(cleanup && (q != NULL)) {
        q->impl->ref_count--;
        q->impl = NULL;
        amxc_llist_it_take(&q->it);
    }
    amxc_var_clean(&retval);
    return q;
}

static int local_netmodel_query_subscribe_update(netmodel_query_impl_t* const impl) {
    int ret = AMXB_ERROR_UNKNOWN;

    SAH_TRACEZ_INFO(ME, "Subscribing for object %s, expr = notification == 'query', handler = %p, priv = %p",
                    impl->object, local_reply_handler, impl);
    ret = amxb_subscription_new(&impl->result,
                                netmodel_get_amxb_bus(),
                                impl->object,
                                "notification == 'query'",
                                local_reply_handler,
                                impl);
    return ret;
}

static int local_netmodel_query_subscribe_removal(netmodel_query_impl_t* const impl) {
    int ret = AMXB_ERROR_UNKNOWN;
    amxd_path_t path;
    amxc_string_t expr;
    char* index_str = NULL;
    uint32_t index = 0;
    const char* obj_path = NULL;

    amxc_string_init(&expr, 0);
    amxd_path_init(&path, impl->object);

    index_str = amxd_path_get_last(&path, true);
    when_str_empty(index_str, exit);
    index = strtoul(index_str, NULL, 0);
    obj_path = amxd_path_get(&path, AMXD_OBJECT_TERMINATE);

    amxc_string_setf(&expr, "notification == 'dm:instance-removed' && "
                     "index == %u", index);

    SAH_TRACEZ_INFO(ME, "Subscribing for object %s, expr = %s, handler = %p, priv = %p",
                    obj_path, amxc_string_get(&expr, 0), local_removal_handler, impl);
    ret = amxb_subscription_new(&impl->removal,
                                netmodel_get_amxb_bus(),
                                obj_path,
                                amxc_string_get(&expr, 0),
                                local_removal_handler,
                                impl);

exit:
    amxc_string_clean(&expr);
    amxd_path_clean(&path);
    free(index_str);
    return ret;
}

static int local_netmodel_query_subscribe(netmodel_query_impl_t* const impl) {
    int ret = AMXB_ERROR_UNKNOWN;
    amxc_var_t retval;
    const amxc_var_t* content = NULL;

    amxc_var_init(&retval);
    when_null(impl, exit);

    // If there are no callbacks to be called, there is no need to subscribe to amx events
    if(amxc_llist_is_empty(&impl->cbs)) {
        ret = AMXB_STATUS_OK;
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Subscribing for query %u updates", impl->query_id);
    ret = local_netmodel_query_subscribe_update(impl);
    when_failed_trace(ret, exit, ERROR, "Failed to add subscription for query updates");

    SAH_TRACEZ_INFO(ME, "Subscribing for query %u removal", impl->query_id);
    ret = local_netmodel_query_subscribe_removal(impl);
    when_failed_trace(ret, exit, ERROR, "Failed to add subscription for query removal");

    SAH_TRACEZ_INFO(ME, "Calling getResult for query %u", impl->query_id);
    ret = amxb_call(netmodel_get_amxb_bus(), impl->object, "getResult", NULL, &retval, 5);
    when_failed_trace(ret, exit, ERROR, "Failed to get result for %s", impl->object);

    content = GETI_ARG(&retval, 0);
    amxc_var_copy(&impl->last_result, content);
    SAH_TRACEZ_INFO(ME, "Calling handler(s) for query %u", impl->query_id);
    local_netmodel_query_impl_call_cbs(impl, impl->object, &impl->last_result);

exit:
    if(ret != 0) {
        local_netmodel_query_unsubscribe(impl);
    }
    amxc_var_clean(&retval);
    return ret;
}

static netmodel_query_t* local_netmodel_query_create(const char* const given_intf,
                                                     const char* const interface,
                                                     const char* const subscriber,
                                                     const char* const cl,
                                                     amxc_var_t* const arguments,
                                                     const uint32_t query_id,
                                                     netmodel_callback_t handler,
                                                     void* const userdata) {
    netmodel_query_t* q = NULL;

    if((NULL == interface) || ('\0' == *interface) ||
       ( NULL == subscriber) || ( '\0' == *subscriber) ||
       ( NULL == cl) || ( '\0' == *cl) ||
       ( NULL == arguments) || (NULL == given_intf)) {
        SAH_TRACEZ_WARNING(ME, "Invalid input arguments");
        return NULL;
    }

    q = local_netmodel_query_new(given_intf, interface, subscriber, cl, arguments,
                                 query_id, handler, userdata);
    when_null(q, exit);

    when_failed(local_netmodel_query_subscribe(q->impl), exit);

    return q;

exit:
    local_netmodel_query_delete(q);
    SAH_TRACEZ_ERROR(ME, "OpenQuery failed (%s)", cl);
    return NULL;
}

static void local_netmodel_query_list_it_close(amxc_llist_it_t* const it) {
    netmodel_query_t* q = amxc_container_of(it, netmodel_query_t, it);

    local_async_close_query(q);
}

static int local_netmodel_query_parse_args(const char* const subscriber,
                                           const char* const cl,
                                           amxc_var_t** args,
                                           const amxc_var_t* const arguments,
                                           va_list variadic_list) {
    int ret = -1;

    when_str_empty_trace(subscriber, exit, ERROR, "Mandatory argument subscriber is NULL or empty");
    when_str_empty_trace(cl, exit, ERROR, "Mandatory argument class is NULL or empty");

    when_failed(amxc_var_new(args), exit);
    amxc_var_set_type(*args, AMXC_VAR_ID_HTABLE);
    if(NULL != arguments) {
        if(amxc_var_type_of(arguments) != AMXC_VAR_ID_HTABLE) {
            SAH_TRACEZ_WARNING(ME, "Invalid var type");
            goto exit;
        }
        when_failed(amxc_var_copy(*args, arguments), exit);
    }
    when_failed(local_copy_valist_into_variant(*args, variadic_list), exit);

    ret = 0;

exit:
    return ret;
}

static PRIVATE resolve_status_t local_resolve_path(const char* const path, amxc_var_t* objects) {
    resolve_status_t status = resolve_status_error;
    amxb_bus_ctx_t* ctx = netmodel_get_amxb_bus();
    amxc_var_t data;
    amxc_var_t* var = NULL;

    amxc_var_init(&data);
    when_null(ctx, exit);

    when_failed(amxb_get(ctx, path, 0, &data, 2), exit);
    var = GETP_ARG(&data, "0");
    when_null(var, exit);

    amxc_var_move(objects, var);
    status = resolve_status_ok;

exit:
    amxc_var_clean(&data);
    return status;
}

static PRIVATE resolve_status_t local_resolve_nm(const char* const path, amxc_var_t* list) {
    resolve_status_t status = resolve_status_error;
    amxc_var_t data;

    amxc_var_init(&data);

    when_failed(local_resolve_path(path, &data), exit);
    amxc_var_for_each(var, &data) {
        amxc_var_add(cstring_t, list, GETP_CHAR(var, "Alias"));
    }

    status = resolve_status_ok;

exit:
    amxc_var_clean(&data);
    return status;
}

static PRIVATE resolve_status_t local_resolve_tr181(const char* const path, amxc_var_t* list) {
    resolve_status_t status = resolve_status_error;
    amxc_var_t* data = NULL;

    data = local_call_netmodel_function_amxc_var_t("resolver", "resolvePath", NULL, "path", AMXC_VAR_ID_CSTRING, path, NULL);
    when_null(data, exit);

    amxc_var_move(list, data);
    if(amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, list))) {
        status = resolve_status_tr181_not_found;
    } else {
        status = resolve_status_ok;
    }

    amxc_var_delete(&data);

exit:
    return status;
}

static void local_resolve_handler(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv) {
    netmodel_query_t* q = (netmodel_query_t*) priv;
    netmodel_query_impl_t* impl = q->impl;
    netmodel_query_cb_t* cb = q->cb;
    netmodel_query_t* new_q = NULL;
    amxc_var_t list;
    amxc_string_t str;

    amxc_string_init(&str, 0);
    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);

    when_true(amxc_var_is_null(data), remove_resolver);
    when_true(amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, data)), exit);

    new_q = local_async_open_query_list(impl->given_intf, data, impl->subscriber, impl->cl,
                                        cb->handler, cb->userdata, &impl->arguments);
    when_null(new_q, remove_resolver);
    amxc_llist_append(&impl->queries, &new_q->it);

remove_resolver:
    amxc_llist_for_each(it, &impl->queries) {
        netmodel_query_t* child = amxc_container_of(it, netmodel_query_t, it);
        if(0 == strcmp(child->impl->interface, "resolver")) {
            local_async_close_query(child);
        }
    }

exit:
    amxc_string_clean(&str);
    amxc_var_clean(&list);
    return;
}

static void local_reply_handler(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                void* const priv) {
    netmodel_query_impl_t* const impl = (netmodel_query_impl_t*) priv;

    when_null_trace(impl, exit, WARNING, "Could not get userdata");

    SAH_TRACEZ_INFO(ME, "Received update for query %u", impl->query_id);
    if(!amxc_llist_is_empty(&impl->cbs)) {
        const char* object = GET_CHAR(data, "object");
        amxc_var_t* result = GET_ARG(data, "Result");
        int ret = -1;
        when_null(object, exit);
        when_null(result, exit);

        if((amxc_var_compare(&impl->last_result, result, &ret) != 0) || (ret != 0)) {
            SAH_TRACEZ_INFO(ME, "Going to call handler(s) for query %u", impl->query_id);
            amxc_var_copy(&impl->last_result, result);
            local_netmodel_query_impl_call_cbs(impl, object, &impl->last_result);
        } else {
            SAH_TRACEZ_INFO(ME, "Query result is the same as previously, not triggering callback(s)");
        }
    }

    local_netmodel_query_check_for_deletion(query_list);

exit:
    return;
}

static void local_removal_handler(UNUSED const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv) {
    netmodel_query_impl_t* const impl = (netmodel_query_impl_t*) priv;

    when_null_trace(impl, exit, WARNING, "Could not get userdata");

    if(!amxc_llist_is_empty(&impl->cbs)) {
        const char* object = GET_CHAR(data, "object");
        amxc_var_t result;

        when_null(object, exit);
        amxc_var_init(&result);

        local_netmodel_query_impl_call_cbs(impl, object, &result);

        amxc_var_clean(&result);
    }

    local_netmodel_query_check_for_deletion(query_list);

exit:
    return;
}

PRIVATE char* local_resolve_first_intf(const char* const path) {
    char* intf = NULL;
    amxc_var_t intfs;

    amxc_var_init(&intfs);

    when_failed(local_resolve_intfs(path, &intfs), exit);
    intf = amxc_var_dyncast(cstring_t, amxc_var_get_first(&intfs));

exit:
    amxc_var_clean(&intfs);
    if(intf == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not find a matching %s Intf for %s",
                         root_obj_name, path);
    }
    return intf;
}

PRIVATE resolve_status_t local_resolve_intfs(const char* const input, amxc_var_t* intfs) {
    resolve_status_t status = resolve_status_error;
    amxc_string_t str;
    amxc_var_t list;
    amxd_path_t path;
    const char* p_str = NULL;

    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);
    amxc_string_init(&str, 0);
    amxd_path_init(&path, (NULL == input || '\0' == *input) ? "lo" : input);

    when_null(intfs, exit);

    // If path is invalid, return
    when_false(amxd_path_is_valid(&path), exit);
    p_str = amxd_path_get(&path, AMXD_OBJECT_TERMINATE);

    // The input is a parameter, assuming Intf name or index -> use as is
    if(strcmp(p_str, ".") == 0) {
        amxc_var_add(cstring_t, &list, amxd_path_get_param(&path));
        goto copy_output;
    }

    // If the given path is not . terminated,
    // try adding a dot and resolve the path
    // Object references are not dot terminated and need this
    if(amxd_path_get_param(&path) != NULL) {
        amxd_path_setf(&path, true, "%s", input);
        p_str = amxd_path_get(&path, AMXD_OBJECT_TERMINATE);
    }

    // Path starts with root_obj.Intf. -> resolve Intfs
    amxc_string_setf(&str, "%s.Intf.", root_obj_name);
    if(strncmp(p_str, amxc_string_get(&str, 0), amxc_string_text_length(&str)) == 0) {
        when_failed(local_resolve_nm(p_str, &list), exit);
        goto copy_output;
    }

    // Assuming TR181 path, find matching NetModel Intfs
    status = local_resolve_tr181(p_str, &list);
    when_failed(status, exit);

copy_output:
    amxc_var_move(intfs, &list);
    status = resolve_status_ok;

exit:
    amxc_string_clean(&str);
    amxc_var_clean(&list);
    amxd_path_clean(&path);
    return status;
}

PRIVATE void local_call_netmodel_function(amxc_var_t* variant_ret,
                                          const char* const interface,
                                          const char* const function_name,
                                          amxc_var_t* variant_args,
                                          va_list variadic_list) {
    amxb_bus_ctx_t* ctx;
    amxc_string_t path;
    bool var_allocated = false;
    const char* intf = (NULL == interface || '\0' == *interface) ? "lo" : interface;

    if((NULL == variant_ret) || (NULL == function_name) || ('\0' == *function_name)) {
        SAH_TRACEZ_WARNING(ME, "Invalid input arguments");
        return;
    }

    amxc_string_init(&path, 0);

    if(NULL == variant_args) {
        amxc_var_new(&variant_args);
        amxc_var_set_type(variant_args, AMXC_VAR_ID_HTABLE);
        var_allocated = true;
    }

    if(amxc_var_type_of(variant_args) != AMXC_VAR_ID_HTABLE) {
        SAH_TRACEZ_WARNING(ME, "Invalid var type");
        goto exit;
    }

    when_failed(local_copy_valist_into_variant(variant_args, variadic_list), exit);

    if(strcmp(intf, root_obj_name) == 0) {
        amxc_string_set(&path, intf);
    } else {
        amxc_string_setf(&path, "%s.Intf.%s", root_obj_name, intf);
    }

    when_null((ctx = netmodel_get_amxb_bus()), exit);

    amxb_call(ctx, amxc_string_get(&path, 0), function_name, variant_args, variant_ret, 2);

exit:
    amxc_string_clean(&path);
    if(var_allocated) {
        amxc_var_delete(&variant_args);
    }
    return;
}

PRIVATE bool local_call_netmodel_function_bool(const char* const interface,
                                               const char* const function_name,
                                               amxc_var_t* const args,
                                               ...) {
    amxc_var_t variant_ret;
    va_list variadic_list;
    bool result = false;

    when_str_empty_trace(function_name, exit, WARNING, "Mandatory argument function_name is NULL or empty");

    amxc_var_init(&variant_ret);

    va_start(variadic_list, args);
    local_call_netmodel_function(&variant_ret, interface, function_name, args, variadic_list);
    va_end(variadic_list);

    if(!amxc_var_is_null(&variant_ret)) {
        result = GETI_BOOL(&variant_ret, 0);
    }

    amxc_var_clean(&variant_ret);

exit:
    return result;
}

PRIVATE void local_call_netmodel_function_void(const char* const interface,
                                               const char* const function_name,
                                               amxc_var_t* const args,
                                               ...) {
    amxc_var_t variant_ret;
    va_list variadic_list;

    when_str_empty_trace(function_name, exit, WARNING, "Mandatory argument function_name is NULL or empty");

    amxc_var_init(&variant_ret);

    va_start(variadic_list, args);
    local_call_netmodel_function(&variant_ret, interface, function_name, args, variadic_list);
    va_end(variadic_list);

    amxc_var_clean(&variant_ret);

exit:
    return;
}

PRIVATE amxc_var_t* local_call_netmodel_function_amxc_var_t(const char* const interface,
                                                            const char* const function_name,
                                                            amxc_var_t* const args,
                                                            ...) {
    amxc_var_t variant_ret;
    va_list variadic_list;
    amxc_var_t* result = NULL;

    when_str_empty_trace(function_name, exit, WARNING, "Mandatory argument function_name is NULL or empty");

    amxc_var_init(&variant_ret);

    va_start(variadic_list, args);
    local_call_netmodel_function(&variant_ret, interface, function_name, args, variadic_list);
    va_end(variadic_list);

    if(!amxc_var_is_null(&variant_ret)) {
        result = amxc_var_take_index(&variant_ret, 0);
    }

    amxc_var_clean(&variant_ret);

exit:
    return result;
}

PRIVATE char* local_call_netmodel_function_cstring(const char* const interface,
                                                   const char* const function_name,
                                                   amxc_var_t* const args,
                                                   ...) {
    amxc_var_t variant_ret;
    va_list variadic_list;
    char* result = NULL;

    when_str_empty_trace(function_name, exit, WARNING, "Mandatory argument function_name is NULL or empty");

    amxc_var_init(&variant_ret);

    va_start(variadic_list, args);
    local_call_netmodel_function(&variant_ret, interface, function_name, args, variadic_list);
    va_end(variadic_list);

    if(!amxc_var_is_null(&variant_ret)) {
        result = amxc_var_dyncast(cstring_t, GETI_ARG(&variant_ret, 0));
    }

    amxc_var_clean(&variant_ret);

exit:
    return result;
}

PRIVATE uint32_t local_call_netmodel_function_uint32_t(const char* const interface,
                                                       const char* const function_name,
                                                       amxc_var_t* const args,
                                                       ...) {
    amxc_var_t variant_ret;
    va_list variadic_list;
    uint32_t result = 0;

    when_str_empty_trace(function_name, exit, WARNING, "Mandatory argument function_name is NULL or empty");

    amxc_var_init(&variant_ret);

    va_start(variadic_list, args);
    local_call_netmodel_function(&variant_ret, interface, function_name, args, variadic_list);
    va_end(variadic_list);

    if(!amxc_var_is_null(&variant_ret)) {
        result = GETI_UINT32(&variant_ret, 0);
    }

    amxc_var_clean(&variant_ret);

exit:
    return result;
}

PRIVATE netmodel_query_t* local_async_open_query(const char* const interface,
                                                 const char* const subscriber,
                                                 const char* const cl,
                                                 netmodel_callback_t handler,
                                                 void* const userdata,
                                                 const amxc_var_t* const arguments,
                                                 ...) {
    netmodel_query_t* q = NULL;
    va_list variadic_list;
    va_start(variadic_list, arguments);
    amxc_var_t* args = NULL;
    amxc_var_t intf_list;
    resolve_status_t status = resolve_status_error;
    netmodel_query_impl_t* impl = NULL;

    amxc_var_init(&intf_list);

    SAH_TRACEZ_INFO(ME, "Opening query on interface %s", interface);

    when_failed(local_netmodel_query_parse_args(subscriber, cl, &args, arguments, variadic_list), exit);

    impl = local_netmodel_query_impl_find(interface, subscriber, cl, args, true);
    if(impl != NULL) {
        q = local_netmodel_query_new_existing_impl(impl, handler, userdata);
        when_null_trace(q, exit, ERROR, "Failed to initialize netmodel query data");
        goto exit;
    }

    status = local_resolve_intfs(interface, &intf_list);
    switch(status) {
    case resolve_status_ok:
        q = local_async_open_query_list(interface, &intf_list, subscriber, cl, handler, userdata, args);
        break;
    case resolve_status_tr181_not_found:
        q = local_async_open_resolve_query(interface, subscriber, cl, handler, userdata, args);
        break;
    case resolve_status_error:
        SAH_TRACEZ_ERROR(ME, "Could not find a matching %s Intf for %s", root_obj_name, interface);
        break;
    default:
        break;
    }

exit:
    va_end(variadic_list);
    amxc_var_delete(&args);
    amxc_var_clean(&intf_list);
    return q;
}

PRIVATE void local_async_close_query(netmodel_query_t* const q) {
    when_null_trace(q, exit, WARNING, "Query is NULL");
    when_null_trace(q->impl, exit, WARNING, "Query implementation information is NULL");

    local_netmodel_query_cb_close(q->cb);
    local_netmodel_query_impl_close(&q->impl);
    if(query_list_locked) {
        q->should_destroy = true;
    } else {
        local_netmodel_query_delete(q);
    }

exit:
    return;
}

void local_init(void) {
    if(query_list == NULL) {
        amxc_llist_new(&query_list);
    }
}

void local_cleanup(void) {
    amxc_llist_delete(&query_list, local_netmodel_query_list_it_close);
}

const char* local_get_root_obj_name(void) {
    return root_obj_name;
}
