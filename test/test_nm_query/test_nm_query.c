/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <unistd.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>

#include "mock.h"
#include "test_nm_query.h"
#include "netmodel/client.h"

static unsigned int cb_count = 0;
static unsigned int cb_count_2 = 0;

typedef struct _query_list_item {
    amxc_llist_it_t it;
    netmodel_query_t** query;
} query_list_item_t;

int test_nm_setup(UNUSED void** state) {
    amxut_bus_setup(state);
    test_init_dummy_fn_resolvers(amxut_bus_parser());
    test_setup_parse_odls();
    assert_true(netmodel_initialize());

    amxut_bus_handle_events();

    return 0;
}

int test_nm_teardown(UNUSED void** state) {
    amxut_bus_teardown(state);
    netmodel_cleanup();

    return 0;
}

static void test_nm_cb_rm(UNUSED const char* sig_name,
                          const amxc_var_t* data,
                          void* priv) {
    netmodel_query_t* q = *(netmodel_query_t**) priv;
    cb_count++;
    assert_non_null(data);
    assert_true(amxc_var_type_of(data) == AMXC_VAR_ID_BOOL);

    if(q != NULL) {
        netmodel_closeQuery(q);
        *(netmodel_query_t**) priv = NULL;
    }
}

static void test_nm_cb_rm_2(UNUSED const char* sig_name,
                            const amxc_var_t* data,
                            void* priv) {
    amxc_llist_t* q_list = (amxc_llist_t*) priv;
    cb_count++;
    assert_non_null(data);
    assert_true(amxc_var_type_of(data) == AMXC_VAR_ID_BOOL);

    amxc_llist_for_each(it, q_list) {
        query_list_item_t* query_list_item = amxc_container_of(it, query_list_item_t, it);
        if(query_list_item != NULL) {
            netmodel_query_t* q = *query_list_item->query;
            if(q != NULL) {
                netmodel_closeQuery(q);
                *query_list_item->query = NULL;
            }
        }
    }
}

static void test_nm_cb(UNUSED const char* sig_name,
                       const amxc_var_t* data,
                       UNUSED void* priv) {
    cb_count++;
    assert_non_null(data);
    if(amxc_var_is_null(data)) {
        assert_string_equal(sig_name, "Resolver");
    } else {
        assert_true(amxc_var_type_of(data) == AMXC_VAR_ID_BOOL);
    }
}

static void test_nm_cb_2(UNUSED const char* sig_name,
                         const amxc_var_t* data,
                         UNUSED void* priv) {
    cb_count_2++;
    assert_non_null(data);
    if(amxc_var_is_null(data)) {
        assert_string_equal(sig_name, "Resolver");
    } else {
        assert_true(amxc_var_type_of(data) == AMXC_VAR_ID_BOOL);
    }
}

static void test_nm_update_query(const char* const intf, bool value) {
    amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), "NetModel.Intf.%s.Query.1.", intf);
    amxc_var_t var;

    amxc_var_init(&var);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &var, "Result", value);

    assert_non_null(obj);

    amxd_object_emit_signal(obj, "query", &var);

    amxut_bus_handle_events();

    amxc_var_clean(&var);
}

static void test_nm_update_resolve_query(const char* const intf) {
    amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), "%s", "NetModel.Intf.resolver.Query.1.");
    amxc_var_t data;
    amxc_var_t var;

    amxc_var_init(&data);
    amxc_var_init(&var);
    assert_non_null(obj);

    amxc_var_set_type(&data, AMXC_VAR_ID_LIST);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);
    amxc_var_add(cstring_t, &data, intf);
    amxc_var_set_key(&var, "Result", &data, AMXC_VAR_FLAG_COPY);

    amxd_object_emit_signal(obj, "query", &var);

    amxut_bus_handle_events();

    amxc_var_clean(&data);
    amxc_var_clean(&var);
}

static bool test_nm_verify_query_cb(const netmodel_query_t* q, const char* intf) {
    bool ret = false;
    unsigned int count = cb_count;

    when_null(q, exit);

    test_nm_update_query(intf, true);
    when_false(cb_count == ++count, exit);

    ret = true;

exit:
    return ret;
}

static void test_nm_verify_query_call(netmodel_query_t* q,
                                      const char* intf,
                                      const fn_counter_t* fn_counter,
                                      unsigned int count_cb,
                                      unsigned int uint32_count,
                                      unsigned int void_count,
                                      bool verify_cb) {
    bool q_verified = false;
    amxc_var_t intfs;
    amxc_string_t str;

    amxc_string_init(&str, 0);
    amxc_var_init(&intfs);

    amxc_string_set(&str, intf);
    amxc_string_ssv_to_var(&str, &intfs, NULL);

    amxut_bus_handle_events();

    assert_true(fn_counter->uint32_count == uint32_count);
    assert_true(cb_count == count_cb);

    amxc_var_for_each(var, &intfs) {
        const char* in = amxc_var_constcast(cstring_t, var);
        q_verified = test_nm_verify_query_cb(q, in);
        if(verify_cb) {
            assert_true(q_verified);
        } else {
            assert_false(q_verified);
        }
    }

    netmodel_closeQuery(q);
    assert_true(fn_counter->void_count == void_count);

    amxc_var_clean(&intfs);
    amxc_string_clean(&str);
}

void test_nm_open_query(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery("intf_0", "sub", "isUp", NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery("intf_0", "", "isUp", NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery("intf_0", "sub", "isUp", NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery("", "sub", "isUp", NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery("intf_0", "sub", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_is_up(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_isUp("intf_0", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_isUp("intf_0", "", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_isUp("intf_0", "sub", NULL, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_isUp("", "sub", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_isUp("wrong_intf", "sub", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_has_flag(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_hasFlag("intf_0", "sub", "some_flag", "cond", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_hasFlag("intf_0", "", "flag", "cond", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_hasFlag("intf_0", "sub", NULL, NULL, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_hasFlag("", "sub", "", "cond", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_hasFlag("wrong_intf", "sub", "cond", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_is_linked_to(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_isLinkedTo("intf_0", "sub", "intf_1", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_isLinkedTo("intf_0", "", "lo", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_isLinkedTo("intf_0", "sub", NULL, NULL, NULL, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_isLinkedTo("", "sub", "intf_1", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_isLinkedTo("wrong_intf", "sub", "intf_1", NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_get_intfs(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_getIntfs("intf_0", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getIntfs("intf_0", "", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getIntfs("intf_0", "sub", NULL, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getIntfs("", "sub", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getIntfs("wrong_intf", "sub", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_lucky_intf(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_luckyIntf("intf_0", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyIntf("intf_0", "", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyIntf("intf_0", "sub", NULL, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyIntf("", "sub", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyIntf("wrong_intf", "sub", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_get_parameters(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_getParameters("intf_0", "sub", "param", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getParameters("intf_0", "", "param", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getParameters("intf_0", "sub", "", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getParameters("intf_0", "sub", "param", NULL, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getParameters("", "sub", "param", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getParameters("wrong_intf", "sub", "param", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_get_first_parameter(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_getFirstParameter("intf_0", "sub", "param", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getFirstParameter("intf_0", "", "param", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getFirstParameter("intf_0", "sub", "", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getFirstParameter("intf_0", "sub", "param", NULL, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getFirstParameter("", "sub", "param", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getFirstParameter("wrong_intf", "sub", "param", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_get_addrs(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_getAddrs("intf_0", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getAddrs("intf_0", "", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getAddrs("intf_0", "sub", NULL, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getAddrs("", "sub", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getAddrs("wrong_intf", "sub", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_lucky_addr(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_luckyAddr("intf_0", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyAddr("intf_0", "", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyAddr("intf_0", "sub", NULL, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyAddr("", "sub", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyAddr("wrong_intf", "sub", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_lucky_addr_address(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_luckyAddrAddress("intf_0", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyAddrAddress("intf_0", "", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyAddrAddress("intf_0", "sub", NULL, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyAddrAddress("", "sub", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_luckyAddrAddress("wrong_intf", "sub", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_get_dhcp_option(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_getDHCPOption("intf_0", "sub", "req", 1, netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getDHCPOption("intf_0", "", "req", 1, netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getDHCPOption("intf_0", "sub", "", 1, netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getDHCPOption("intf_0", "sub", "req", 1, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getDHCPOption("", "sub", "req", 1, netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getDHCPOption("wrong_intf", "sub", "req", 1, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_get_ipv6_prefixes(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_getIPv6Prefixes("intf_0", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getIPv6Prefixes("intf_0", "", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getIPv6Prefixes("intf_0", "sub", NULL, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getIPv6Prefixes("", "sub", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getIPv6Prefixes("wrong_intf", "sub", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_nm_open_query_get_first_ipv6_prefix(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_getFirstIPv6Prefix("intf_0", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getFirstIPv6Prefix("intf_0", "", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getFirstIPv6Prefix("intf_0", "sub", NULL, NULL, NULL, NULL);
    assert_non_null(q);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, ++uint32_count, ++void_count, false);

    count_cb = cb_count;
    q = netmodel_openQuery_getFirstIPv6Prefix("", "sub", "", netmodel_traverse_down, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "lo", count_ptr, ++count_cb, ++uint32_count, ++void_count, true);

    count_cb = cb_count;
    q = netmodel_openQuery_getFirstIPv6Prefix("wrong_intf", "sub", NULL, NULL, test_nm_cb, NULL);
    test_nm_verify_query_call(q, "intf_0", count_ptr, count_cb, uint32_count, void_count, false);
}

void test_multi_intf_resolving(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;

    q = netmodel_openQuery_isUp("NetModel.Intf.*.", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    count_cb += 4;
    uint32_count += 4;
    void_count += 4;
    test_nm_verify_query_call(q, "intf_0 lo resolver bridge-bridge", count_ptr, count_cb, uint32_count, void_count, true);
    count_cb = cb_count;

    q = netmodel_openQuery_isUp("Device.IP.Interface.*.", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    count_cb += 2;
    uint32_count += 2;
    void_count += 2;
    test_nm_verify_query_call(q, "intf_0 lo", count_ptr, count_cb, uint32_count, void_count, true);
    count_cb = cb_count;

    q = netmodel_openQuery_isUp("IP.Interface.*.", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    count_cb += 2;
    uint32_count += 2;
    void_count += 2;
    test_nm_verify_query_call(q, "intf_0 lo", count_ptr, count_cb, uint32_count, void_count, true);
    count_cb = cb_count;

    q = netmodel_openQuery_isUp("Device.IP.Interface.[Alias != 'wrong'].", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    count_cb += 2;
    uint32_count += 2;
    void_count += 2;
    test_nm_verify_query_call(q, "intf_0 lo", count_ptr, count_cb, uint32_count, void_count, true);
    count_cb = cb_count;

    q = netmodel_openQuery_isUp("Device.IP.Interface.[Alias in '[ip_intf_0 ip_lo]'].", "sub", "some_flag", netmodel_traverse_down, test_nm_cb, NULL);
    count_cb += 2;
    uint32_count += 2;
    void_count += 2;
    test_nm_verify_query_call(q, "intf_0 lo", count_ptr, count_cb, uint32_count, void_count, true);
    count_cb = cb_count;
}

void test_nm_open_identical_queries(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int bool_count = count_ptr->bool_count;
    unsigned int count_cb = cb_count;
    unsigned int count_cb_2 = cb_count_2;
    netmodel_query_t* q = NULL;
    netmodel_query_t* q2 = NULL;

    q = netmodel_openQuery("intf_0", "sub", "isUp", NULL, test_nm_cb, NULL);
    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(count_ptr->bool_count, ++bool_count);
    q2 = netmodel_openQuery("intf_0", "sub", "isUp", NULL, test_nm_cb, NULL);
    assert_int_equal(count_ptr->bool_count, bool_count);
    test_nm_verify_query_call(q2, "intf_0", count_ptr, ++count_cb, ++uint32_count, void_count, true);
    assert_int_equal(cb_count, ++count_cb);

    netmodel_closeQuery(q);
    assert_int_equal(count_ptr->void_count, ++void_count);

    q = netmodel_openQuery("intf_0", "sub", "isUp", NULL, test_nm_cb, NULL);
    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(count_ptr->bool_count, ++bool_count);
    q2 = netmodel_openQuery("intf_0", "sub", "isUp", NULL, test_nm_cb_2, NULL);
    assert_int_equal(cb_count_2, ++count_cb_2);
    assert_int_equal(count_ptr->bool_count, bool_count);

    test_nm_update_query("intf_0", true);
    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(cb_count_2, ++count_cb_2);

    netmodel_closeQuery(q);
    assert_int_equal(count_ptr->void_count, void_count);

    q = netmodel_openQuery("intf_0", "sub", "isUp", NULL, NULL, NULL);
    assert_int_equal(cb_count, count_cb);
    assert_int_equal(count_ptr->bool_count, bool_count);

    test_nm_update_query("intf_0", false);
    assert_int_equal(cb_count, count_cb);
    assert_int_equal(cb_count_2, ++count_cb_2);

    netmodel_closeQuery(q2);
    assert_int_equal(count_ptr->void_count, void_count);

    netmodel_closeQuery(q);
    assert_int_equal(count_ptr->void_count, ++void_count);
}

void test_nm_close_query_in_cb(UNUSED void** state) {
    netmodel_query_t* q = NULL;
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int bool_count = count_ptr->bool_count;
    unsigned int count_cb = cb_count;

    q = netmodel_openQuery("intf_0", "sub", "isUp", NULL, test_nm_cb_rm, &q);
    assert_non_null(q);
    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(count_ptr->bool_count, ++bool_count);
    assert_int_equal(count_ptr->uint32_count, ++uint32_count);

    test_nm_update_query("intf_0", true);
    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(count_ptr->void_count, ++void_count);
    assert_null(q);
}


void test_nm_close_identical_queries_in_cb(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int bool_count = count_ptr->bool_count;
    unsigned int count_cb = cb_count;
    netmodel_query_t* q = NULL;
    netmodel_query_t* q2 = NULL;
    netmodel_query_t* q3 = NULL;
    netmodel_query_t* q4 = NULL;
    amxc_llist_t queries_to_del;
    query_list_item_t q_item;
    query_list_item_t q2_item;

    amxc_llist_init(&queries_to_del);
    amxc_llist_it_init(&q_item.it);
    amxc_llist_it_init(&q2_item.it);

    q = netmodel_openQuery("intf_0", "sub", "isUp", NULL, test_nm_cb, NULL);
    assert_non_null(q);
    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(count_ptr->bool_count, ++bool_count);
    q_item.query = &q;
    amxc_llist_append(&queries_to_del, &q_item.it);

    q2 = netmodel_openQuery("intf_0", "sub", "isUp", NULL, test_nm_cb, NULL);
    assert_non_null(q2);
    assert_int_equal(count_ptr->bool_count, bool_count);
    assert_int_equal(cb_count, ++count_cb);
    q2_item.query = &q2;
    amxc_llist_append(&queries_to_del, &q2_item.it);

    q3 = netmodel_openQuery("intf_0", "other_sub", "isUp", NULL, test_nm_cb, NULL);
    assert_non_null(q3);
    assert_int_equal(count_ptr->bool_count, ++bool_count);
    assert_int_equal(cb_count, ++count_cb);

    q4 = netmodel_openQuery("intf_0", "other_sub", "isUp", NULL, test_nm_cb_rm_2, &queries_to_del);
    assert_non_null(q4);
    assert_int_equal(count_ptr->bool_count, bool_count);
    assert_int_equal(cb_count, ++count_cb);

    netmodel_closeQuery(q4);
    assert_null(q);
    assert_null(q2);

    test_nm_update_query("intf_0", true);
    netmodel_closeQuery(q3);

    amxc_llist_clean(&queries_to_del, NULL);
}

void test_nm_resolver_query(UNUSED void** state) {
    netmodel_query_t* q = NULL;
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int bool_count = count_ptr->bool_count;
    unsigned int count_cb = cb_count;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    q = netmodel_openQuery("Device.IP.Interface.3.", "sub", "isUp", NULL, test_nm_cb, NULL);
    assert_non_null(q);
    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(count_ptr->bool_count, ++bool_count);
    assert_int_equal(count_ptr->uint32_count, ++uint32_count);

    netmodel_closeQuery(q);
    assert_int_equal(count_ptr->void_count, ++void_count);

    q = netmodel_openQuery("Device.IP.Interface.3.", "sub", "isUp", NULL, test_nm_cb, NULL);
    assert_non_null(q);
    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(count_ptr->bool_count, ++bool_count);
    assert_int_equal(count_ptr->uint32_count, ++uint32_count);

    amxd_trans_select_pathf(&trans, "NetModel.Intf.");
    amxd_trans_add_inst(&trans, 0, "intf_resolve");
    amxd_trans_set_value(cstring_t, &trans, "InterfacePath", "Device.IP.Interface.3.");
    amxd_trans_select_pathf(&trans, "NetModel.Intf.intf_resolve.Query.");
    amxd_trans_add_inst(&trans, 1, NULL);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), amxd_status_ok);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    test_nm_update_resolve_query("intf_resolve");

    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(count_ptr->bool_count, ++bool_count);
    assert_int_equal(count_ptr->uint32_count, ++uint32_count);
    assert_int_equal(count_ptr->void_count, ++void_count);

    netmodel_closeQuery(q);
    assert_int_equal(count_ptr->void_count, ++void_count);
}

void test_nm_identical_resolver_query(UNUSED void** state) {
    netmodel_query_t* q = NULL;
    netmodel_query_t* q2 = NULL;
    netmodel_query_t* q3 = NULL;
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int uint32_count = count_ptr->uint32_count;
    unsigned int void_count = count_ptr->void_count;
    unsigned int bool_count = count_ptr->bool_count;
    unsigned int count_cb = cb_count;
    unsigned int count_cb_2 = cb_count_2;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    q = netmodel_openQuery("Device.IP.Interface.4.", "sub", "isUp", NULL, test_nm_cb, NULL);
    assert_non_null(q);
    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(count_ptr->bool_count, ++bool_count);
    assert_int_equal(count_ptr->uint32_count, ++uint32_count);

    q2 = netmodel_openQuery("Device.IP.Interface.4.", "sub", "isUp", NULL, test_nm_cb_2, NULL);
    assert_non_null(q2);
    assert_int_equal(cb_count_2, ++count_cb_2);
    assert_int_equal(count_ptr->bool_count, bool_count);
    assert_int_equal(count_ptr->uint32_count, uint32_count);

    amxd_trans_select_pathf(&trans, "NetModel.Intf.");
    amxd_trans_add_inst(&trans, 0, "intf_resolve_2");
    amxd_trans_set_value(cstring_t, &trans, "InterfacePath", "Device.IP.Interface.4.");
    amxd_trans_select_pathf(&trans, "NetModel.Intf.intf_resolve_2.Query.");
    amxd_trans_add_inst(&trans, 1, NULL);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), amxd_status_ok);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    test_nm_update_resolve_query("intf_resolve_2");

    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(cb_count_2, ++count_cb_2);
    assert_int_equal(count_ptr->bool_count, ++bool_count);
    assert_int_equal(count_ptr->uint32_count, ++uint32_count);
    assert_int_equal(count_ptr->void_count, ++void_count);

    netmodel_closeQuery(q);
    assert_int_equal(count_ptr->void_count, void_count);

    netmodel_closeQuery(q2);
    assert_int_equal(count_ptr->void_count, ++void_count);

    amxd_trans_init(&trans);

    q = netmodel_openQuery("Device.IP.Interface.5.", "sub", "isUp", NULL, test_nm_cb, NULL);
    assert_non_null(q);
    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(count_ptr->bool_count, ++bool_count);
    assert_int_equal(count_ptr->uint32_count, ++uint32_count);

    q2 = netmodel_openQuery("Device.IP.Interface.5.", "sub", "getIntfs", NULL, test_nm_cb_2, NULL);
    assert_non_null(q2);
    assert_int_equal(cb_count_2, ++count_cb_2);
    assert_int_equal(count_ptr->bool_count, bool_count);
    assert_int_equal(count_ptr->uint32_count, uint32_count);

    amxd_trans_select_pathf(&trans, "NetModel.Intf.");
    amxd_trans_add_inst(&trans, 0, "intf_resolve_3");
    amxd_trans_set_value(cstring_t, &trans, "InterfacePath", "Device.IP.Interface.5.");
    amxd_trans_select_pathf(&trans, "NetModel.Intf.intf_resolve_3.Query.");
    amxd_trans_add_inst(&trans, 1, NULL);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), amxd_status_ok);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    test_nm_update_resolve_query("intf_resolve_3");

    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(cb_count_2, ++count_cb_2);
    bool_count += 2;
    assert_int_equal(count_ptr->bool_count, bool_count);
    uint32_count += 2;
    assert_int_equal(count_ptr->uint32_count, uint32_count);
    assert_int_equal(count_ptr->void_count, ++void_count);

    q3 = netmodel_openQuery("Device.IP.Interface.5.", "sub", "isUp", NULL, test_nm_cb, NULL);
    assert_int_equal(cb_count, ++count_cb);
    assert_int_equal(count_ptr->bool_count, bool_count);
    assert_int_equal(count_ptr->uint32_count, uint32_count);

    netmodel_closeQuery(q);
    assert_int_equal(count_ptr->void_count, void_count);

    netmodel_closeQuery(q2);
    assert_int_equal(count_ptr->void_count, ++void_count);

    netmodel_closeQuery(q3);
    assert_int_equal(count_ptr->void_count, ++void_count);

}
