/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "test_nm_query.h"

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_nm_open_query),
        cmocka_unit_test(test_nm_open_query_is_up),
        cmocka_unit_test(test_nm_open_query_has_flag),
        cmocka_unit_test(test_nm_open_query_is_linked_to),
        cmocka_unit_test(test_nm_open_query_get_intfs),
        cmocka_unit_test(test_nm_open_query_lucky_intf),
        cmocka_unit_test(test_nm_open_query_get_parameters),
        cmocka_unit_test(test_nm_open_query_get_first_parameter),
        cmocka_unit_test(test_nm_open_query_get_addrs),
        cmocka_unit_test(test_nm_open_query_lucky_addr),
        cmocka_unit_test(test_nm_open_query_lucky_addr_address),
        cmocka_unit_test(test_nm_open_query_get_dhcp_option),
        cmocka_unit_test(test_nm_open_query_get_ipv6_prefixes),
        cmocka_unit_test(test_nm_open_query_get_first_ipv6_prefix),
        cmocka_unit_test(test_multi_intf_resolving),
        cmocka_unit_test(test_nm_open_identical_queries),
        cmocka_unit_test(test_nm_close_query_in_cb),
        cmocka_unit_test(test_nm_close_identical_queries_in_cb),
        cmocka_unit_test(test_nm_resolver_query),
        cmocka_unit_test(test_nm_identical_resolver_query),
    };
    return cmocka_run_group_tests(tests, test_nm_setup, test_nm_teardown);
}
