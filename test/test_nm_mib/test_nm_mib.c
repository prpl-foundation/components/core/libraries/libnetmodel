/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <unistd.h>
#include <cmocka.h>
#include <string.h>
#include <poll.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include <amxd/amxd_transaction.h>

#include "mock.h"
#include "test_nm_mib.h"
#include "netmodel/client.h"
#include "netmodel/mib.h"
#include "amxut/amxut_timer.h"

int test_nm_setup(UNUSED void** state) {
    amxut_bus_setup(state);
    test_init_dummy_fn_resolvers(amxut_bus_parser());
    test_setup_parse_odls();
    assert_true(netmodel_initialize());

    return 0;
}

int test_nm_teardown(UNUSED void** state) {
    netmodel_cleanup();
    amxut_bus_teardown(state);

    return 0;
}

static amxs_status_t add_parameters(amxs_sync_ctx_t* ctx) {
    return amxs_sync_ctx_add_new_copy_param(ctx, "Value", "Value", AMXS_SYNC_DEFAULT);
}

static char* find_instance(amxb_bus_ctx_t* bus, const char* interface_path) {
    char* instance = NULL;
    assert_non_null(bus);
    assert_string_equal(interface_path, "Device.IP.Interface.1.");
    instance = strdup(interface_path); // to test if value is freed by lib_netmodel
    assert_non_null(instance);
    return instance;
}

static char* find_object_b(const char* netmodel_intf) {
    amxc_string_t str;
    char* obj_b = NULL;

    amxc_string_init(&str, 0);

    amxc_string_setf(&str, "%sIPv6Router.", netmodel_intf);
    obj_b = amxc_string_take_buffer(&str);

    amxc_string_clean(&str);
    return obj_b;
}

static void set_dm_default_values() {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, amxd_dm_findf(amxut_bus_dm(), "NetModel.Intf.intf_0."));
    amxd_trans_set_value(cstring_t, &trans, "Status_ext", "");
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_set_value(uint32_t, &trans, "Value", 0);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, amxd_dm_findf(amxut_bus_dm(), "IP.Interface.ip_intf_0."));
    amxd_trans_set_value(cstring_t, &trans, "Status", "Up");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(uint32_t, &trans, "Value", 42);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "Device.IP.Interface.ip_lo.");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, amxd_dm_findf(amxut_bus_dm(), "Bridging.Bridge.bridge."));
    amxd_trans_set_value(cstring_t, &trans, "Port", "Device.IP.Interface.ip_lo.");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();
}

void test_nm_subscribe(UNUSED void** state) {
    amxc_var_t data;
    netmodel_mib_t mib = {
        .root = "IP.",
        .find_instance_to_sync = find_instance,
    };
    netmodel_mib_t bad_mib = {
        .find_instance_to_sync = find_instance,
    };
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int count = count_ptr->link_count;

    amxc_var_init(&data);

    set_dm_default_values();

    // bad subscribe
    assert_false(netmodel_subscribe(NULL, NULL));
    assert_false(netmodel_subscribe(&data, NULL));
    assert_false(netmodel_subscribe(NULL, &mib));
    assert_false(netmodel_subscribe(&data, &mib));
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.intf_0.");
    assert_false(netmodel_subscribe(&data, &mib));
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.1.");
    assert_false(netmodel_subscribe(&data, &mib));
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.intf_0.");
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.1.");
    assert_false(netmodel_subscribe(&data, &bad_mib));
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.intf_0.");
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.1.");
    assert_false(netmodel_subscribe(&data, NULL));

    // good subscribe
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.intf_0.");
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.1.");
    assert_true(netmodel_subscribe(&data, &mib));
    assert_int_equal(count_ptr->link_count, ++count);
    assert_string_equal(test_get_last_linked_ulintf(), "intf_0");
    assert_string_equal(test_get_last_linked_llintf(), "lo");

    // bad unsubscribe
    netmodel_unsubscribe(NULL, NULL);
    assert_int_equal(count_ptr->link_count, count);
    netmodel_unsubscribe(&data, NULL);
    assert_int_equal(count_ptr->link_count, count);
    netmodel_unsubscribe(NULL, &mib);
    assert_int_equal(count_ptr->link_count, count);

    // good unsubscribe
    netmodel_unsubscribe(&data, &mib);
    assert_int_equal(count_ptr->link_count, --count);

//cleanup:
    amxc_var_clean(&data);
}

void test_nm_flag_dont_link_interfaces(UNUSED void** state) {
    amxc_var_t data;
    netmodel_mib_t mib = {
        .root = "IP.",
        .flags = NETMODEL_MIB_DONT_LINK_INTERFACES,
    };
    amxc_var_t params;
    amxd_object_t* netmodel_intf = NULL;
    amxd_trans_t trans;
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int count = count_ptr->link_count;

    amxc_var_init(&params);
    amxc_var_init(&data);
    amxd_trans_init(&trans);

    netmodel_intf = amxd_dm_findf(amxut_bus_dm(), "NetModel.Intf.intf_0.");
    assert_non_null(netmodel_intf);

    // check initial values
    set_dm_default_values();
    assert_int_equal(amxd_object_get_params(netmodel_intf, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status_ext"), "");
    assert_int_equal(GET_BOOL(&params, "Enable"), false);
    assert_int_equal(GET_UINT32(&params, "Value"), 0);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.intf_0.");
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.1.");
    assert_true(netmodel_subscribe(&data, &mib));
    assert_int_equal(count_ptr->link_count, count);

    // check values after sync
    assert_int_equal(amxd_object_get_params(netmodel_intf, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status_ext"), "Up");
    assert_int_equal(GET_BOOL(&params, "Enable"), true);
    assert_int_equal(GET_UINT32(&params, "Value"), 0); // no add_parameters callback defined in netmodel_mib_t so not synced

    // try to trigger netmodel_unlinkIntfs
    amxd_trans_select_pathf(&trans, "IP.Interface.ip_intf_0.");
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    assert_int_equal(count_ptr->link_count, count);

    netmodel_unsubscribe(&data, &mib);

//cleanup:
    amxc_var_clean(&data);
    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_nm_flag_only_my_parameters(UNUSED void** state) {
    amxc_var_t data;
    netmodel_mib_t mib = {
        .root = "IP.",
        .flags = NETMODEL_MIB_ONLY_MY_PARAMETERS,
        .add_sync_parameters = add_parameters,
    };
    amxc_var_t params;
    amxd_object_t* netmodel_intf = NULL;
    amxd_trans_t trans;
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int count = count_ptr->link_count;

    amxc_var_init(&params);
    amxc_var_init(&data);
    amxd_trans_init(&trans);

    netmodel_intf = amxd_dm_findf(amxut_bus_dm(), "NetModel.Intf.intf_0.");
    assert_non_null(netmodel_intf);

    // check initial values
    set_dm_default_values();
    assert_int_equal(amxd_object_get_params(netmodel_intf, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status_ext"), "");
    assert_int_equal(GET_BOOL(&params, "Enable"), false);
    assert_int_equal(GET_UINT32(&params, "Value"), 0);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.intf_0.");
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.1.");
    assert_true(netmodel_subscribe(&data, &mib));
    assert_int_equal(count_ptr->link_count, ++count);
    assert_string_equal(test_get_last_linked_ulintf(), "intf_0");
    assert_string_equal(test_get_last_linked_llintf(), "lo");

    // check values after sync
    assert_int_equal(amxd_object_get_params(netmodel_intf, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status_ext"), ""); // not added with add_parameters callback so not synced
    assert_int_equal(GET_BOOL(&params, "Enable"), false);     // not added with add_parameters callback so not synced
    assert_int_equal(GET_UINT32(&params, "Value"), 42);

    // trigger netmodel_unlinkIntfs
    amxd_trans_select_pathf(&trans, "IP.Interface.ip_intf_0.");
    amxd_trans_set_value(cstring_t, &trans, "LowerLayers", "");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();
    assert_int_equal(count_ptr->link_count, --count);

    netmodel_unsubscribe(&data, &mib);

//cleanup:
    amxc_var_clean(&data);
    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_nm_flag_link_as_lower_layer(UNUSED void** state) {
    netmodel_mib_t mib = {
        .root = "Bridging.",
        .interfaces_parameter = "Port",
        .flags = NETMODEL_MIB_LINK_AS_LOWER_LAYER,
    };
    amxc_var_t data;
    amxd_object_t* netmodel_intf = NULL;
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int count = count_ptr->link_count;

    amxc_var_init(&data);
    set_dm_default_values();

    netmodel_intf = amxd_dm_findf(amxut_bus_dm(), "NetModel.Intf.bridge-bridge.");
    assert_non_null(netmodel_intf);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.bridge-bridge.");
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.4.");
    assert_true(netmodel_subscribe(&data, &mib));
    assert_int_equal(count_ptr->link_count, ++count);
    assert_string_equal(test_get_last_linked_ulintf(), "lo");
    assert_string_equal(test_get_last_linked_llintf(), "bridge-bridge");  // because of flag NETMODEL_MIB_LINK_AS_LOWER_LAYER

    netmodel_unsubscribe(&data, &mib);
    assert_int_equal(count_ptr->link_count, --count);

//cleanup:
    amxc_var_clean(&data);
}

void test_nm_find_object_b(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t params;
    amxd_object_t* obj_b = NULL;
    netmodel_mib_t mib = {
        .root = "IP.",
        .flags = NETMODEL_MIB_ONLY_MY_PARAMETERS,
        .add_sync_parameters = add_parameters,
        .find_object_b_to_sync = find_object_b,
    };

    amxc_var_init(&data);
    amxc_var_init(&params);

    set_dm_default_values();

    obj_b = amxd_dm_findf(amxut_bus_dm(), "NetModel.Intf.intf_0.IPv6Router");
    assert_non_null(obj_b);

    // check initial values
    set_dm_default_values();
    assert_int_equal(amxd_object_get_params(obj_b, &params, amxd_dm_access_protected), 0);
    assert_int_equal(GET_UINT32(&params, "Value"), 0);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.intf_0.");
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.1.");
    assert_true(netmodel_subscribe(&data, &mib));

    // check values after sync
    assert_int_equal(amxd_object_get_params(obj_b, &params, amxd_dm_access_protected), 0);
    assert_int_equal(GET_UINT32(&params, "Value"), 42);

    netmodel_unsubscribe(&data, &mib);

//cleanup:
    amxc_var_clean(&data);
    amxc_var_clean(&params);
}

static void create_subscription(UNUSED amxb_bus_ctx_t* bus, UNUSED const char* interface_path, amxc_string_t* object, amxc_string_t* expression) {
    amxc_string_appendf(expression, " && path == 'IP.Interface.' && parameters.Alias == 'ip_intf_0'");
    amxc_string_set(object, "IP.Interface.");
}

static void reset_parameters(amxs_sync_ctx_t* ctx) {
    amxd_trans_t trans;
    const char* netmodel_intf = ctx->b;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, amxd_dm_findf(amxut_bus_dm(), netmodel_intf));
    amxd_trans_set_value(uint32_t, &trans, "Value", 0);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
}

void test_nm_create_subscription_for_object_a(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* obj_b = NULL;
    netmodel_mib_t mib = {
        .root = "IP.",
        .flags = NETMODEL_MIB_DONT_LINK_INTERFACES,
        .create_subscription_for_object_a = create_subscription,
        .add_sync_parameters = add_parameters,
        .reset_sync_parameters = reset_parameters,
    };

    amxc_var_init(&data);
    amxc_var_init(&params);

    set_dm_default_values();

    obj_b = amxd_dm_findf(amxut_bus_dm(), "NetModel.Intf.intf_0.");
    assert_non_null(obj_b);

    print_message("start sync NetModel.Intf.intf_0. and IP.Interface.ip_intf_0.\n");
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.intf_0.");
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.1.");
    assert_true(netmodel_subscribe(&data, &mib));

    amxut_bus_handle_events();
    assert_int_equal(amxd_object_get_params(obj_b, &params, amxd_dm_access_protected), 0);
    assert_int_equal(GET_UINT32(&params, "Value"), 42);

    print_message("remove object A, the sync context's object A should be stale and value should be reset to 0\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.");
    amxd_trans_del_inst(&trans, 0, "ip_intf_0");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    assert_int_equal(amxd_object_get_params(obj_b, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status_ext"), "Up");
    assert_int_equal(GET_UINT32(&params, "Value"), 0);

    print_message("add object A, the sync context should use this new object\n");
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.");
    amxd_trans_add_inst(&trans, 0, "ip_intf_0");
    amxd_trans_set_value(cstring_t, &trans, "Status", "Waiting");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);

    amxut_bus_handle_events();

    assert_int_equal(amxd_object_get_params(obj_b, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status_ext"), "Waiting");

    print_message("stop sync\n");
    netmodel_unsubscribe(&data, &mib);

    amxut_bus_handle_events();

//cleanup:
    amxc_var_clean(&data);
    amxc_var_clean(&params);
}

static char* find_retry_instance(UNUSED amxb_bus_ctx_t* bus, UNUSED const char* interface_path) {
    char* instance = NULL;
    amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), "IP.Interface.retry");
    when_null(obj, exit);

    instance = amxd_object_get_path(obj, AMXD_OBJECT_TERMINATE | AMXD_OBJECT_INDEXED);
    assert_non_null(instance);

exit:
    return instance;
}

void test_nm_sync_retry(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t params;
    netmodel_mib_t mib = {
        .root = "IP.",
        .add_sync_parameters = add_parameters,
        .find_instance_to_sync = find_retry_instance,
    };
    amxd_trans_t trans;
    amxd_object_t* obj = NULL;

    amxc_var_init(&data);
    amxc_var_init(&params);

    // Add NM intf with InterfacePath IP.4
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NetModel.Intf.");
    amxd_trans_add_inst(&trans, 0, "retry");
    amxd_trans_set_value(cstring_t, &trans, "InterfacePath", "Device.IP.Interface.4.");
    amxd_trans_set_value(uint32_t, &trans, "Value", 0);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    // Subscribe
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, "object", "NetModel.Intf.retry");
    amxc_var_add_key(cstring_t, &data, "path", "NetModel.Intf.5.");
    assert_true(netmodel_subscribe(&data, &mib));
    amxut_bus_handle_events();

    // Veriy that sync is not started
    obj = amxd_dm_findf(amxut_bus_dm(), "NetModel.Intf.retry");
    assert_non_null(obj);
    assert_int_equal(amxd_object_get_params(obj, &params, amxd_dm_access_protected), 0);
    assert_int_equal(GET_UINT32(&params, "Value"), 0);

    // Add IP Interface instance
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "IP.Interface.");
    amxd_trans_add_inst(&trans, 0, "retry");
    amxd_trans_set_value(uint32_t, &trans, "Value", 1337);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
    amxut_bus_handle_events();

    // Wait for retry timer, which is set at 10s initially
    amxut_timer_go_to_future_ms(11000);

    // Check if sync is ok
    assert_int_equal(amxd_object_get_params(obj, &params, amxd_dm_access_protected), 0);
    assert_int_equal(GET_UINT32(&params, "Value"), 1337);

    // Unsubscribe
    netmodel_unsubscribe(&data, &mib);

    amxc_var_clean(&data);
    amxc_var_clean(&params);
}
