/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <unistd.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>

#include "mock.h"
#include "test_nm_func.h"
#include "netmodel/client.h"

int test_nm_setup(UNUSED void** state) {
    amxut_bus_setup(state);
    test_init_dummy_fn_resolvers(amxut_bus_parser());
    test_setup_parse_odls();
    assert_true(netmodel_initialize());

    amxut_bus_handle_events();

    return 0;
}

int test_nm_teardown(UNUSED void** state) {
    amxut_bus_teardown(state);
    netmodel_cleanup();

    return 0;
}

void test_nm_is_up(UNUSED void** state) {
    assert_true(netmodel_isUp("intf_0", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("intf_0", NULL, NULL));
    assert_false(netmodel_isUp("wrong_intf", NULL, NULL));
    assert_true(netmodel_isUp(NULL, NULL, NULL));
}

void test_nm_has_flag(UNUSED void** state) {
    assert_true(netmodel_hasFlag("intf_0", "some_flag", "", netmodel_traverse_this));
    assert_true(netmodel_hasFlag("intf_0", "some_flag", NULL, netmodel_traverse_this));
    assert_true(netmodel_hasFlag("intf_0", "some_flag", "my_condition", NULL));
    assert_false(netmodel_hasFlag("wrong_intf", "some_flag", "my_condition", NULL));
    assert_true(netmodel_hasFlag("intf_0", "", "", netmodel_traverse_this));
    assert_true(netmodel_hasFlag("intf_0", NULL, "", netmodel_traverse_this));
    assert_true(netmodel_hasFlag(NULL, "some_flag", "", netmodel_traverse_this));
}

void test_nm_set_flag(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int count = count_ptr->void_count;

    netmodel_setFlag("intf_0", "my_flag", "", netmodel_traverse_this);
    assert_true(count_ptr->void_count == ++count);
    netmodel_setFlag("intf_0", "my_flag", NULL, netmodel_traverse_this);
    assert_true(count_ptr->void_count == ++count);
    netmodel_setFlag("wrong_intf", "my_flag", NULL, netmodel_traverse_this);
    assert_true(count_ptr->void_count == count);
    netmodel_setFlag("intf_0", NULL, NULL, netmodel_traverse_this);
    assert_true(count_ptr->void_count == count);
    netmodel_setFlag(NULL, "my_flag", NULL, netmodel_traverse_this);
    assert_true(count_ptr->void_count == ++count);
}

void test_nm_clear_flag(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int count = count_ptr->void_count;

    netmodel_clearFlag("intf_0", "my_flag", "", netmodel_traverse_this);
    assert_true(count_ptr->void_count == ++count);
    netmodel_clearFlag("intf_0", "my_flag", NULL, netmodel_traverse_this);
    assert_true(count_ptr->void_count == ++count);
    netmodel_clearFlag("wrong_intf", "my_flag", NULL, netmodel_traverse_this);
    assert_true(count_ptr->void_count == count);
    netmodel_clearFlag("intf_0", NULL, NULL, netmodel_traverse_this);
    assert_true(count_ptr->void_count == count);
    netmodel_clearFlag(NULL, "my_flag", NULL, netmodel_traverse_this);
    assert_true(count_ptr->void_count == ++count);
}

void test_nm_is_linked_to(UNUSED void** state) {
    assert_true(netmodel_isLinkedTo("intf_0", "some_intf", netmodel_traverse_this));
    assert_true(netmodel_isLinkedTo("intf_0", "some_intf", NULL));
    assert_false(netmodel_isLinkedTo("intf_0", NULL, NULL));
    assert_false(netmodel_isLinkedTo("wrong_intf", "some_intf", NULL));
    assert_false(netmodel_isLinkedTo(NULL, NULL, NULL));
    assert_true(netmodel_isLinkedTo(NULL, "some_intf", netmodel_traverse_this));
}

static bool test_nm_verify_llist(amxc_var_t* var) {
    bool ret = false;
    amxc_var_t* value = NULL;

    when_null(var, exit);
    when_false(amxc_var_type_of(var) == AMXC_VAR_ID_LIST, exit);
    value = amxc_var_get_first(var);
    when_null(value, exit);
    when_failed(strcmp("intf_0", amxc_var_constcast(cstring_t, value)), exit);

    value = amxc_var_get_next(value);
    when_null(value, exit);
    when_failed(strcmp("lo", amxc_var_constcast(cstring_t, value)), exit);

    ret = true;

exit:
    amxc_var_delete(&var);
    return ret;
}

void test_nm_get_intfs(UNUSED void** state) {
    amxc_var_t* intfs = NULL;

    intfs = netmodel_getIntfs("intf_0", "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_llist(intfs));
    intfs = netmodel_getIntfs("intf_0", NULL, NULL);
    assert_true(test_nm_verify_llist(intfs));
    intfs = netmodel_getIntfs("wrong_intf", "some_flag", netmodel_traverse_down);
    assert_null(intfs);
    intfs = netmodel_getIntfs(NULL, "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_llist(intfs));
}

void test_nm_lucky_intf(UNUSED void** state) {
    char* intf = NULL;

    intf = netmodel_luckyIntf("intf_0", "some_flag", netmodel_traverse_down);
    assert_true(NULL != intf && strcmp(intf, "intf_0") == 0);
    free(intf);
    intf = netmodel_luckyIntf("intf_0", NULL, NULL);
    assert_true(NULL != intf && strcmp(intf, "intf_0") == 0);
    free(intf);
    intf = netmodel_luckyIntf("wrong_intf", "some_flag", netmodel_traverse_down);
    assert_null(intf);
    free(intf);
    intf = netmodel_luckyIntf(NULL, "some_flag", netmodel_traverse_down);
    assert_true(NULL != intf && strcmp(intf, "intf_0") == 0);
    free(intf);
}

static bool test_nm_verify_htable(amxc_var_t* var) {
    bool ret = false;
    amxc_var_t* value = NULL;

    when_null(var, exit);
    when_false(amxc_var_type_of(var) == AMXC_VAR_ID_HTABLE, exit);
    value = amxc_var_get_key(var, "intf_0", AMXC_VAR_FLAG_DEFAULT);
    when_null(value, exit);
    when_failed(strcmp("intf_0", amxc_var_constcast(cstring_t, value)), exit);

    value = amxc_var_get_key(var, "lo", AMXC_VAR_FLAG_DEFAULT);
    when_null(value, exit);
    when_failed(strcmp("lo", amxc_var_constcast(cstring_t, value)), exit);

    ret = true;

exit:
    amxc_var_delete(&var);
    return ret;
}

void test_nm_get_parameters(UNUSED void** state) {
    amxc_var_t* params = NULL;

    params = netmodel_getParameters("intf_0", "name", "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_htable(params));
    params = netmodel_getParameters("intf_0", "name", NULL, NULL);
    assert_true(test_nm_verify_htable(params));
    params = netmodel_getParameters("intf_0", NULL, "some_flag", netmodel_traverse_down);
    assert_null(params);
    params = netmodel_getParameters("wrong_intf", "name", "some_flag", netmodel_traverse_down);
    assert_null(params);
    params = netmodel_getParameters(NULL, "name", "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_htable(params));
}

static bool test_nm_verify_bool(amxc_var_t* var) {
    bool ret = false;

    when_null(var, exit);
    when_false(amxc_var_type_of(var) == AMXC_VAR_ID_BOOL, exit);

    ret = amxc_var_constcast(bool, var);

exit:
    amxc_var_delete(&var);
    return ret;
}

void test_nm_get_first_parameter(UNUSED void** state) {
    amxc_var_t* param = NULL;

    param = netmodel_getFirstParameter("intf_0", "name", "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_bool(param));
    param = netmodel_getFirstParameter("intf_0", "name", NULL, NULL);
    assert_true(test_nm_verify_bool(param));
    param = netmodel_getFirstParameter("intf_0", NULL, "some_flag", netmodel_traverse_down);
    assert_null(param);
    param = netmodel_getFirstParameter("wrong_intf", "name", "some_flag", netmodel_traverse_down);
    assert_null(param);
    param = netmodel_getFirstParameter(NULL, "name", "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_bool(param));
}

void test_nm_set_parameters(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int count = count_ptr->void_count;
    amxc_var_t* param = NULL;
    amxc_var_new(&param);

    amxc_var_set(cstring_t, param, "some_parameter");
    netmodel_setParameters("intf_0", "name", param, "some_flag", netmodel_traverse_down);
    assert_true(count_ptr->void_count == ++count);
    netmodel_setParameters("intf_0", "name", param, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == ++count);
    netmodel_setParameters("", "name", param, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == ++count);
    netmodel_setParameters("wrong_intf", "name", param, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == count);
    netmodel_setParameters("wrong_intf", "", param, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == count);
    netmodel_setParameters("intf_0", NULL, param, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == count);
    netmodel_setParameters("intf_0", "name", NULL, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == count);

    amxc_var_delete(&param);
}

void test_nm_set_first_parameter(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int count = count_ptr->void_count;
    amxc_var_t* param = NULL;
    amxc_var_new(&param);

    amxc_var_set(cstring_t, param, "some_parameter");
    netmodel_setFirstParameter("intf_0", "name", param, "some_flag", netmodel_traverse_down);
    assert_true(count_ptr->void_count == ++count);
    netmodel_setFirstParameter("intf_0", "name", param, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == ++count);
    netmodel_setFirstParameter("", "name", param, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == ++count);
    netmodel_setFirstParameter("wrong_intf", "name", param, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == count);
    netmodel_setFirstParameter("wrong_intf", "", param, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == count);
    netmodel_setFirstParameter("intf_0", NULL, param, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == count);
    netmodel_setFirstParameter("intf_0", "name", NULL, NULL, netmodel_traverse_down);
    assert_true(count_ptr->void_count == count);

    amxc_var_delete(&param);
}

void test_nm_get_addrs(UNUSED void** state) {
    amxc_var_t* addrs = NULL;

    addrs = netmodel_getAddrs("intf_0", "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_llist(addrs));
    addrs = netmodel_getAddrs("intf_0", NULL, NULL);
    assert_true(test_nm_verify_llist(addrs));
    addrs = netmodel_getAddrs("wrong_intf", "some_flag", netmodel_traverse_down);
    assert_null(addrs);
    addrs = netmodel_getAddrs(NULL, "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_llist(addrs));
}

void test_nm_lucky_addr(UNUSED void** state) {
    amxc_var_t* addr = NULL;

    addr = netmodel_luckyAddr("intf_0", "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_htable(addr));
    addr = netmodel_luckyAddr("intf_0", NULL, NULL);
    assert_true(test_nm_verify_htable(addr));
    addr = netmodel_luckyAddr("wrong_intf", "some_flag", netmodel_traverse_down);
    assert_null(addr);
    addr = netmodel_luckyAddr(NULL, "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_htable(addr));
}

void test_nm_lucky_addr_address(UNUSED void** state) {
    char* addr = NULL;

    addr = netmodel_luckyAddrAddress("intf_0", "some_flag", netmodel_traverse_down);
    assert_true(NULL != addr && strcmp(addr, "intf_0") == 0);
    free(addr);
    addr = netmodel_luckyAddrAddress("intf_0", NULL, NULL);
    assert_true(NULL != addr && strcmp(addr, "intf_0") == 0);
    free(addr);
    addr = netmodel_luckyAddrAddress("wrong_intf", "some_flag", netmodel_traverse_down);
    assert_null(addr);
    free(addr);
    addr = netmodel_luckyAddrAddress(NULL, "some_flag", netmodel_traverse_down);
    assert_true(NULL != addr && strcmp(addr, "intf_0") == 0);
    free(addr);
}

void test_nm_get_dhcp_option(UNUSED void** state) {
    amxc_var_t* option = NULL;

    option = netmodel_getDHCPOption("intf_0", "req", 1, netmodel_traverse_down);
    assert_true(test_nm_verify_htable(option));
    option = netmodel_getDHCPOption("intf_0", NULL, 1, netmodel_traverse_down);
    assert_null(option);
    option = netmodel_getDHCPOption("wrong_intf", "req", 1, netmodel_traverse_down);
    assert_null(option);
    option = netmodel_getDHCPOption(NULL, "req", 1, netmodel_traverse_down);
    assert_true(test_nm_verify_htable(option));
}

void test_nm_get_mibs(UNUSED void** state) {
    amxc_var_t* mibs = NULL;

    mibs = netmodel_getMibs("intf_0", "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_htable(mibs));
    mibs = netmodel_getMibs("intf_0", NULL, NULL);
    assert_true(test_nm_verify_htable(mibs));
    mibs = netmodel_getMibs("wrong_intf", "some_flag", netmodel_traverse_down);
    assert_null(mibs);
    mibs = netmodel_getMibs(NULL, "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_htable(mibs));
}

void test_nm_get_ipv6_prefixes(UNUSED void** state) {
    amxc_var_t* prefixes = NULL;

    prefixes = netmodel_getIPv6Prefixes("intf_0", "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_llist(prefixes));
    prefixes = netmodel_getIPv6Prefixes("intf_0", NULL, NULL);
    assert_true(test_nm_verify_llist(prefixes));
    prefixes = netmodel_getIPv6Prefixes("wrong_intf", "some_flag", netmodel_traverse_down);
    assert_null(prefixes);
    prefixes = netmodel_getIPv6Prefixes(NULL, "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_llist(prefixes));
}

void test_nm_get_first_ipv6_prefix(UNUSED void** state) {
    amxc_var_t* prefix = NULL;

    prefix = netmodel_getFirstIPv6Prefix("intf_0", "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_htable(prefix));
    prefix = netmodel_getFirstIPv6Prefix("intf_0", NULL, NULL);
    assert_true(test_nm_verify_htable(prefix));
    prefix = netmodel_getFirstIPv6Prefix("wrong_intf", "some_flag", netmodel_traverse_down);
    assert_null(prefix);
    prefix = netmodel_getFirstIPv6Prefix(NULL, "some_flag", netmodel_traverse_down);
    assert_true(test_nm_verify_htable(prefix));
}

void test_nm_link_intfs(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int count = count_ptr->void_count;

    netmodel_linkIntfs("intf_0", "intf_1");
    assert_true(count_ptr->void_count == ++count);
    netmodel_linkIntfs("intf_0", "");
    assert_true(count_ptr->void_count == count);
    netmodel_linkIntfs("", "intf_1");
    assert_true(count_ptr->void_count == count);
    netmodel_linkIntfs(NULL, "intf_1");
    assert_true(count_ptr->void_count == count);
    netmodel_linkIntfs("intf_0", NULL);
    assert_true(count_ptr->void_count == count);
}

void test_nm_unlink_intfs(UNUSED void** state) {
    const fn_counter_t* count_ptr = test_get_fn_counter();
    unsigned int count = count_ptr->void_count;

    netmodel_unlinkIntfs("intf_0", "intf_1");
    assert_true(count_ptr->void_count == ++count);
    netmodel_unlinkIntfs("intf_0", "");
    assert_true(count_ptr->void_count == count);
    netmodel_unlinkIntfs("", "intf_1");
    assert_true(count_ptr->void_count == count);
    netmodel_unlinkIntfs(NULL, "intf_1");
    assert_true(count_ptr->void_count == count);
    netmodel_unlinkIntfs("intf_0", NULL);
    assert_true(count_ptr->void_count == count);
}

void test_single_intf_resolving(UNUSED void** state) {
    assert_true(netmodel_isUp("NetModel.Intf.intf_0.", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("NetModel.Intf.intf_0", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("NetModel.Intf.lo.", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("NetModel.Intf.lo", "some_flag", netmodel_traverse_this));
    assert_false(netmodel_isUp("NetModel.Intf.wrong", "some_flag", netmodel_traverse_this));

    assert_true(netmodel_isUp("Device.IP.Interface.1.", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("Device.IP.Interface.ip_intf_0.", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("Device.IP.Interface.2.", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("Device.IP.Interface.ip_lo.", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("Device.IP.Interface.ip_lo", "some_flag", netmodel_traverse_this));
    assert_false(netmodel_isUp("Device.IP.Interface.wrong", "some_flag", netmodel_traverse_this));
    assert_false(netmodel_isUp("Device.this.is.wrong.", "some_flag", netmodel_traverse_this));

    assert_true(netmodel_isUp("IP.Interface.1.", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("IP.Interface.ip_intf_0.", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("IP.Interface.2.", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("IP.Interface.ip_lo.", "some_flag", netmodel_traverse_this));

    assert_true(netmodel_isUp("IP.Interface.[Alias == 'ip_intf_0'].", "some_flag", netmodel_traverse_this));
    assert_true(netmodel_isUp("IP.Interface.[Alias == 'ip_lo'].", "some_flag", netmodel_traverse_this));
}
