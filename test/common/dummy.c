/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>

#include <amxo/amxo.h>

#include <amxb/amxb.h>

#include "mock.h"

static const char* const odl_mock_netmodel = "../common/mock_netmodel.odl";
static const char* const odl_mock_ip = "../common/mock_ip.odl";
static fn_counter_t fn_count = {0, 0, 0, 0, 0, 0, 0};
static amxc_htable_t* queries = NULL;
static char upper_layer[200] = "";
static char lower_layer[200] = "";

typedef struct {
    amxc_htable_it_t it;
    char* subscriber;
    char* path;
} dummy_query_t;

static amxd_status_t _dummy_void(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    fn_count.void_count++;

    return amxd_status_ok;
}

static amxd_status_t _dummy_list(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 amxc_var_t* ret) {
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, ret, "intf_0");
    amxc_var_add(cstring_t, ret, "lo");
    fn_count.list_count++;

    return amxd_status_ok;
}

static amxd_status_t _dummy_htable(UNUSED amxd_object_t* object,
                                   UNUSED amxd_function_t* func,
                                   UNUSED amxc_var_t* args,
                                   amxc_var_t* ret) {
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_t* value = amxc_var_add_new_key(ret, "intf_0");
    amxc_var_set(cstring_t, value, "intf_0");
    value = amxc_var_add_new_key(ret, "lo");
    amxc_var_set(cstring_t, value, "lo");
    fn_count.htable_count++;

    return amxd_status_ok;
}

static amxd_status_t _dummy_str(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                amxc_var_t* ret) {
    amxc_var_set(cstring_t, ret, "intf_0");
    fn_count.str_count++;

    return amxd_status_ok;
}

static amxd_status_t _dummy_bool(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 amxc_var_t* ret) {
    amxc_var_set(bool, ret, true);
    fn_count.bool_count++;

    return amxd_status_ok;
}

static amxd_status_t _dummy_uint32(UNUSED amxd_object_t* object,
                                   UNUSED amxd_function_t* func,
                                   UNUSED amxc_var_t* args,
                                   amxc_var_t* ret) {
    amxc_var_set(uint32_t, ret, 1);
    fn_count.uint32_count++;

    return amxd_status_ok;
}

static amxd_status_t _getResult(amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                UNUSED amxc_var_t* args,
                                amxc_var_t* ret) {
    amxd_status_t status = amxd_status_ok;
    char* netmodel_intf_path = NULL;

    // Very poor implementation to support "netmodel_openQuery("NetModel.Intf.resolver.", "resolvePath", ...)".
    // For subscribers with 'mib' this function returns a list, otherwise it does the legacy behavior.
    netmodel_intf_path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    if(strstr(netmodel_intf_path, "resolver") != NULL) {
        dummy_query_t* q = NULL;
        char* end_of_path = strstr(netmodel_intf_path, ".Query");
        if(end_of_path != NULL) {
            *end_of_path = '\0';
        }
        q = amxc_htable_it_get_data(amxc_htable_get(queries, netmodel_intf_path), dummy_query_t, it);
        if((q != NULL) && (strstr(q->subscriber, "mib") != NULL)) {
            amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
            if(strstr(q->path, "ip_lo") != NULL) {
                amxc_var_add(cstring_t, ret, "lo");
            } else if(strstr(q->path, "bridge") != NULL) {
                amxc_var_add(cstring_t, ret, "bridge-bridge");
            } else {
                amxc_var_add(cstring_t, ret, "intf_0");
            }
            goto exit;
        }
    }

    // legacy behavior
    amxc_var_set(bool, ret, false);
    fn_count.bool_count++;

exit:
    free(netmodel_intf_path);
    return status;
}

static amxd_status_t _openQuery(amxd_object_t* object,
                                amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    amxd_status_t status = _dummy_uint32(object, func, args, ret); // legacy behavior
    dummy_query_t* q = NULL;
    char* path = NULL;

    if(queries == NULL) {
        amxc_htable_new(&queries, 0);
    }

    /*
       2 benefits of allocating memory on heap in openQuery:
       - need to save "subscriber" for resolvePath queries
       - assure closeQuery is called for every openQuery (else a memleak is detectable with valgrind)
     */
    q = calloc(1, sizeof(*q));
    q->subscriber = strdup(GETP_CHAR(args, "subscriber"));
    if(GET_CHAR(args, "path")) {
        q->path = strdup(GET_CHAR(args, "path"));
    }
    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    amxc_htable_insert(queries, path, &q->it);

    free(path);
    return status;
}

static amxd_status_t _closeQuery(amxd_object_t* object,
                                 amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    char* path = NULL;
    amxc_htable_it_t* it = NULL;

    path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    it = amxc_htable_take(queries, path);
    if(it != NULL) {
        dummy_query_t* q = amxc_htable_it_get_data(it, dummy_query_t, it);
        amxc_htable_it_clean(&q->it, NULL);
        it = NULL;
        free(q->subscriber);
        free(q->path);
        free(q);
        q = NULL;
        if(amxc_htable_is_empty(queries)) {
            amxc_htable_delete(&queries, NULL);
        }
    }

    free(path);

    return _dummy_void(object, func, args, ret); // legacy behavior
}

static void resolve_path(amxb_bus_ctx_t* ctx,
                         const char* const path,
                         amxc_var_t* list) {
    amxc_var_t data;
    amxc_var_t* objects = NULL;

    amxc_var_init(&data);
    when_null(ctx, exit);

    when_failed(amxb_get(ctx, path, 0, &data, 2), exit);
    objects = GETP_ARG(&data, "0");
    when_null(objects, exit);

    amxc_var_for_each(var, objects) {
        amxc_llist_t nm_list;

        amxc_llist_init(&nm_list);
        amxd_dm_resolve_pathf(amxut_bus_dm(),
                              &nm_list,
                              "NetModel.Intf.['Device.%s' in InterfacePath]",
                              amxc_var_key(var));

        amxc_llist_iterate(it, &nm_list) {
            amxd_object_t* obj = amxd_dm_findf(amxut_bus_dm(), "%s", amxc_string_get(amxc_string_from_llist_it(it), 0));
            amxc_var_add(cstring_t, list, amxd_object_get_name(obj, AMXD_OBJECT_NAMED));
        }
        amxc_llist_clean(&nm_list, amxc_string_list_it_free);
    }

exit:
    amxc_var_clean(&data);
}

static amxd_status_t _resolvePath(UNUSED amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t list;
    amxd_path_t path;
    char* fixed_part = NULL;
    char* first = NULL;
    amxb_bus_ctx_t* ctx = NULL;

    amxc_var_init(&list);
    amxc_var_set_type(&list, AMXC_VAR_ID_LIST);

    status = amxd_path_init(&path, GET_CHAR(args, "path"));
    when_failed(status, exit);

    first = amxd_path_get_first(&path, false);
    if((first != NULL) && (strcmp("Device.", first) == 0)) {
        free(first);
        first = amxd_path_get_first(&path, true);
    }

    fixed_part = amxd_path_get_fixed_part(&path, false);
    ctx = amxb_be_who_has(fixed_part);
    when_null(ctx, exit_ok);

    resolve_path(ctx, amxd_path_get(&path, AMXD_OBJECT_TERMINATE), &list);

exit_ok:
    amxc_var_copy(ret, &list);
    status = amxd_status_ok;

exit:
    free(fixed_part);
    free(first);
    amxc_var_clean(&list);
    amxd_path_clean(&path);
    return status;
}

static amxd_status_t _linkIntfs(amxd_object_t* object,
                                amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    fn_count.link_count++;
    snprintf(upper_layer, sizeof(upper_layer) - 1, "%s", GET_CHAR(args, "ulintf"));
    snprintf(lower_layer, sizeof(lower_layer) - 1, "%s", GET_CHAR(args, "llintf"));

    return _dummy_void(object, func, args, ret); // legacy behavior;
}

static amxd_status_t _unlinkIntfs(amxd_object_t* object,
                                  amxd_function_t* func,
                                  amxc_var_t* args,
                                  amxc_var_t* ret) {
    fn_count.link_count--;
    upper_layer[0] = '\0';
    lower_layer[0] = '\0';

    return _dummy_void(object, func, args, ret); // legacy behavior;
}

void test_init_dummy_fn_resolvers(amxo_parser_t* parser) {
    amxo_resolver_ftab_add(parser, "linkIntfs", AMXO_FUNC(_linkIntfs));
    amxo_resolver_ftab_add(parser, "unlinkIntfs", AMXO_FUNC(_unlinkIntfs));
    amxo_resolver_ftab_add(parser, "setFlag", AMXO_FUNC(_dummy_void));
    amxo_resolver_ftab_add(parser, "clearFlag", AMXO_FUNC(_dummy_void));
    amxo_resolver_ftab_add(parser, "hasFlag", AMXO_FUNC(_dummy_bool));
    amxo_resolver_ftab_add(parser, "getResult", AMXO_FUNC(_getResult));
    amxo_resolver_ftab_add(parser, "openQuery", AMXO_FUNC(_openQuery));
    amxo_resolver_ftab_add(parser, "closeQuery", AMXO_FUNC(_closeQuery));
    amxo_resolver_ftab_add(parser, "isUp", AMXO_FUNC(_dummy_bool));
    amxo_resolver_ftab_add(parser, "isLinkedTo", AMXO_FUNC(_dummy_bool));
    amxo_resolver_ftab_add(parser, "getIntfs", AMXO_FUNC(_dummy_list));
    amxo_resolver_ftab_add(parser, "luckyIntf", AMXO_FUNC(_dummy_str));
    amxo_resolver_ftab_add(parser, "getParameters", AMXO_FUNC(_dummy_htable));
    amxo_resolver_ftab_add(parser, "setParameters", AMXO_FUNC(_dummy_void));
    amxo_resolver_ftab_add(parser, "getFirstParameter", AMXO_FUNC(_dummy_bool));
    amxo_resolver_ftab_add(parser, "setFirstParameter", AMXO_FUNC(_dummy_void));
    amxo_resolver_ftab_add(parser, "getAddrs", AMXO_FUNC(_dummy_list));
    amxo_resolver_ftab_add(parser, "luckyAddr", AMXO_FUNC(_dummy_htable));
    amxo_resolver_ftab_add(parser, "luckyAddrAddress", AMXO_FUNC(_dummy_str));
    amxo_resolver_ftab_add(parser, "resolvePath", AMXO_FUNC(_resolvePath));
    amxo_resolver_ftab_add(parser, "getDHCPOption", AMXO_FUNC(_dummy_htable));
    amxo_resolver_ftab_add(parser, "getMibs", AMXO_FUNC(_dummy_htable));
    amxo_resolver_ftab_add(parser, "getIPv6Prefixes", AMXO_FUNC(_dummy_list));
    amxo_resolver_ftab_add(parser, "getFirstIPv6Prefix", AMXO_FUNC(_dummy_htable));
}

static void parse_odl(const char* odl_file_name) {
    amxd_object_t* root_obj = amxd_dm_get_root(amxut_bus_dm());
    if(0 != amxo_parser_parse_file(amxut_bus_parser(), odl_file_name, root_obj)) {
        fail_msg("PARSER MESSAGE = %s", amxc_string_get(&amxut_bus_parser()->msg, 0));
    }
}

void test_setup_parse_odls(void) {
    parse_odl(odl_mock_ip);
    parse_odl(odl_mock_netmodel);
}

const fn_counter_t* test_get_fn_counter(void) {
    return &fn_count;
}

const char* test_get_last_linked_ulintf(void) {
    return &upper_layer[0];
}

const char* test_get_last_linked_llintf(void) {
    return &lower_layer[0];
}
