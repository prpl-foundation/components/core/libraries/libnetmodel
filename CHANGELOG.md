# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.5.19 - 2024-09-05(07:41:48 +0000)

### Other

- [NetModel] Connect via direct socket if possible

## Release v1.5.18 - 2024-07-05(13:08:18 +0000)

### Other

- PRPl/Coverity: libnetmodel issue

## Release v1.5.17 - 2024-03-07(13:54:37 +0000)

### Other

- [Lib NetModel] Add the correct license files

## Release v1.5.16 - 2023-12-11(11:43:59 +0000)

### Changes

- [NetModel] Add support for non dot terminated reference paths

## Release v1.5.15 - 2023-11-27(15:52:47 +0000)

### Fixes

- Routing manager crashes due to double free in libnetmodel

## Release v1.5.14 - 2023-09-07(12:35:12 +0000)

### Fixes

- [NetModel] Do not call reset_sync_parameters if ctx is NULL

## Release v1.5.13 - 2023-07-12(13:37:22 +0000)

### Other

- [libnetmodel] fix unhandled signal in tests

## Release v1.5.12 - 2023-03-21(14:00:42 +0000)

### Fixes

- [NetModel] NetDev information in netmodel ppp-wan interface is not cleared when ppp connection stops

## Release v1.5.11 - 2023-03-15(10:54:38 +0000)

### Other

- Still invalid prefix in the Prefixes parameter [Other]

## Release v1.5.10 - 2023-01-12(14:27:38 +0000)

### Fixes

- [NetModel] Queries are not properly closed in netmodel_cleanup

## Release v1.5.9 - 2022-12-22(12:25:08 +0000)

### Fixes

- Do not call a query callback if the result is the same

## Release v1.5.8 - 2022-11-28(15:36:12 +0000)

### Other

- [NetModel] Update copyright information

## Release v1.5.7 - 2022-11-18(15:54:49 +0000)

### Fixes

- [NetModel] Interface linking sometimes fails

## Release v1.5.6 - 2022-11-10(15:12:06 +0000)

### Changes

- [NetModel] Synchronize the Name parameter by default

## Release v1.5.5 - 2022-11-07(12:20:09 +0000)

### Fixes

- gcc 11.2.0 linker cannot find -lsahtrace

## Release v1.5.4 - 2022-11-03(08:14:58 +0000)

### Fixes

- [NetModel] Close resolvePath queries in netmodel clients when the path is resolved

## Release v1.5.3 - 2022-10-13(09:31:03 +0000)

### Fixes

- [NetModel] Optimize identical queries in lib netmodel

## Release v1.5.2 - 2022-08-08(11:07:56 +0000)

### Fixes

- [Lib NetModel] Add a retry mechanism in case mib sync fails to start

## Release v1.5.1 - 2022-06-24(16:20:08 +0000)

### Other

- Fix makefile always using "NeMo" as root object name

## Release v1.5.0 - 2022-06-24(14:20:29 +0000)

### New

- [Lib NetModel] Add API to retrieve the root object name

## Release v1.4.4 - 2022-05-31(07:34:31 +0000)

### Fixes

- [NetModel] NetDevName doesn't sync if the interface does not yet exist during find_instance_to_sync_callback

## Release v1.4.3 - 2022-05-03(12:43:39 +0000)

### Other

- [NetModel] Change query updated event to match the event sent by NeMo

## Release v1.4.2 - 2022-04-22(15:39:27 +0000)

### Fixes

- netdev-up flag on vlan-vlan201 is gone after switching WANManager's WANMode twice

## Release v1.4.1 - 2022-04-21(14:38:13 +0000)

### Changes

- [Lib NetModel] Add an initialized check for each API method

## Release v1.4.0 - 2022-04-19(12:00:44 +0000)

### New

- [netmodel] Support of prefix queries in netmodel/libnetmodel

## Release v1.3.2 - 2022-04-06(06:22:03 +0000)

### Fixes

- [NetModel] Change query updated event to match the event sent by NeMo

## Release v1.3.1 - 2022-03-17(15:37:41 +0000)

### Fixes

- [Lib NetModel] Use amxb_be_who_has instead of own implementation

## Release v1.3.0 - 2022-03-17(09:00:23 +0000)

### New

- implement iprouter client and iprouter mib

## Release v1.2.0 - 2022-02-22(13:52:15 +0000)

### New

- [NetModel] Add getMibs API

## Release v1.1.0 - 2022-02-07(12:36:41 +0000)

### New

- [netmodel] Support getDHCPOption() queries

## Release v1.0.1 - 2022-01-31(11:09:52 +0000)

### Changes

- Integrate support for bridging in NetModel

## Release v1.0.0 - 2022-01-27(08:06:23 +0000)

### Breaking

- wrong mib can be unsubscribed when netmodel clients share a netmodel interface

## Release v0.4.1 - 2022-01-25(15:23:38 +0000)

### Fixes

- parameters not synced after reboot

## Release v0.4.0 - 2022-01-21(12:28:27 +0000)

### New

- expand libnetmodel for mibs/netmodel-clients

## Release v0.3.0 - 2022-01-14(13:22:41 +0000)

### New

- Support queries on TR181 paths that are not yet present

## Release v0.2.0 - 2021-12-15(11:35:25 +0000)

### New

- Add IP address query functions

### Other

- Add documentation generation

## Release v0.1.3 - 2021-10-23(07:09:14 +0000)

### Changes

- [library NetModel] Support TR181 paths

## Release v0.1.2 - 2021-09-12(06:57:35 +0000)

### Other

- [library NetModel] Add open query functions

## Release v0.1.1 - 2021-08-13(07:25:31 +0000)

### Fixes

- Fix traces

## Release v0.1.0 - 2021-08-12(08:30:09 +0000)

### New

- [library NetModel] Initial import

### Other

- Add changenlog

