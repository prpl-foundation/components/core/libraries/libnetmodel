/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __NETMODEL_MIB_H__
#define __NETMODEL_MIB_H__

/**
   @defgroup mib mib.h - Mib Library

   @brief
   The netmodel mib library is a collection of utilities to facilitate the implementation (the mib_*.so file) of NetModel mibs.

   @details
   Typically a mib will implement 2 event handlers:
   - <b>_mib_added</b> on event "dm:mib-added"
   - <b>_mib_removed</b> on event "dm:mib-removed"

   When the mib is added it should call @ref netmodel_subscribe. When the mib is removed it should call @ref netmodel_unsubscribe.

   @attention
   Before doing anything whith this library the mib implementation has to call @ref netmodel_initialize. When the mib implementation is done it has to call @ref netmodel_cleanup.
 */

#include <amxs/amxs.h>

/**
   @ingroup mib
   @defgroup mib_flags flags

   @brief
   The mib library provides flags to tune its behavior.

   @attention
   To keep the configuration to a minimal the default must be good for the majority of mib implementations. This is why most flags disable behavior.
 */

/**
   @ingroup mib_flags
   @brief
   This flag is used to disable linking of the upper and lower layer interfaces.
 */
#define NETMODEL_MIB_DONT_LINK_INTERFACES 1

/**
   @ingroup mib_flags
   @brief
   This flag will prevent the library from synchronising common parameters like Status and Enable.
 */
#define NETMODEL_MIB_ONLY_MY_PARAMETERS 2

/**
   @ingroup mib_flags
   @brief
   This flag is set when the library should link the interface as lower layer.
 */
#define NETMODEL_MIB_LINK_AS_LOWER_LAYER 4

/**
   @ingroup mib
   @defgroup mib_callbacks callback functions

   @brief
   The mib library uses callback functions to interact with the mib implementation.
 */

/**
   @ingroup mib_callbacks
   @brief
   The library will ask the mib implementation which parameters need to be synchronized.

   @attention
   This callback is optional.

   @param[in] ctx Sync context to which parameters can be added.

   @returns amxs_status_ok on success.
 */
typedef amxs_status_t (* add_parameters_t)(amxs_sync_ctx_t* ctx);

/**
   @ingroup mib_callbacks
   @brief
   The library will ask the mib implementation to return the path to the object to be synchronized.

   @attention
   This callback is optional. For most mib implementations the path is parameter NetModel.Intf.{i}.InterfacePath.

   @param[in] bus Backend bus which can be used to look for instances.
   @param[in] interface_path The value of parameter NetModel.Intf.{i}.InterfacePath.

   @returns char* instance path, will be freed when not needed anymore.
 */
typedef char* (* find_instance_t)(amxb_bus_ctx_t* bus, const char* interface_path);

/**
   @ingroup mib_callbacks
   @brief
   The library will ask the mib implementation to return the path to the sub object of the NetModel.Intf.{i}. to be synchronized.

   @attention
   This callback is optional. For most mib implementations the path is NetModel.Intf.{i}.

   @param[in] netmodel_intf The path of NetModel.Intf.{i}.

   @returns char* object path, will be freed when not needed anymore.
 */
typedef char* (* find_object_b_t)(const char* netmodel_intf);

/**
   @ingroup mib_callbacks
   @brief
   The library will ask the mib implementation on what events it should subscribe.
   The event should indicate when object A of the sync context is stale.
   On event, the sync context will be renewed with the event's object.

   @attention
   This callback is optional.

   @param[in] bus Backend bus which can be used to look for instances.
   @param[in] interface_path The value of parameter NetModel.Intf.{i}.InterfacePath.
   @param[out] object Object or search path, object paths must end with a ".".
   @param[out] expression An expression used to filter the events. Default is "notification in ['dm:instance-added','dm:instance-removed']".
 */
typedef void (* create_subscription_t)(amxb_bus_ctx_t* bus, const char* interface_path, amxc_string_t* object, amxc_string_t* expression);

/**
   @ingroup mib_callbacks
   @brief
   The library will ask the mib implementation to reset the synchronized parameters to their default value.

   @attention
   This callback is optional.

   @param[in] ctx Sync context to which parameters can be added.
 */
typedef void (* reset_parameters_t)(amxs_sync_ctx_t* ctx);

/**
   @ingroup mib
   @brief
   The mib struct definition.

   This structure is used to configure the mib library. It holds information fields and callbacks functions.
 */
typedef struct {
    const char* name;                                          /**< Optional, to identify the mib. The default value is 'mib-name-unknown'. */
    const char* root;                                          /**< Root object to find the backend bus. */
    const char* interfaces_parameter;                          /**< Optional, name of the parameter that holds a csv list of interfaces to link with. The default value is 'LowerLayers'. */
    uint32_t flags;                                            /**< Optional, see @ref mib_flags. */
    add_parameters_t add_sync_parameters;                      /**< Optional, see @ref add_parameters_t. */
    find_instance_t find_instance_to_sync;                     /**< Optional, see @ref find_instance_t. */
    find_object_b_t find_object_b_to_sync;                     /**< Optional, see @ref find_object_b_t. */
    create_subscription_t create_subscription_for_object_a;    /**< Optional, see @ref create_subscription_t. */
    reset_parameters_t reset_sync_parameters;                  /**< Optional, see @ref reset_parameters_t. */
} netmodel_mib_t;

/**
   @ingroup mib
   @brief
   Subscribe on NetModel.Intf.{i}.InterfacePath and start to synchronise the mib's parameters.

   @param[in] data A hash table that holds at least 'path' (NetModel.Intf.{i}. indexed path) and 'object' (NetModel.Intf.{i}. path with aliases).
   @param[in] mib Information about the mib and how the library should behave.

   @return True if subscribed successfully.
 */
bool netmodel_subscribe(const amxc_var_t* const data, netmodel_mib_t* mib);

/**
   @ingroup mib
   @brief
   Unsubscribe from NetModel.Intf.{i}.InterfacePath and stop synchronising the mib's parameters.

   @param[in] data A hash table that holds at least 'object' (NetModel.Intf.{i}. path with aliases).
   @param[in] mib Information about the mib and how the library should behave.
 */
void netmodel_unsubscribe(const amxc_var_t* const data, netmodel_mib_t* mib);

#endif // __NETMODEL_MIB_H__
