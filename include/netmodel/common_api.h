/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NETMODEL_COMMON_API_H__
#define __NETMODEL_COMMON_API_H__

/**
   @defgroup common_api common_api.h - Common API
   @{

   @brief
   The NetModel Common API is an C-style version of a subset of the NetModel Data Model API.

   @details
   This API has two implementations: one in the
   NetModel Client Library (see @ref client) and one in the NetModel Core Library (see @ref core) to maximize the availability of the API.
   Applications can use the NetModel Client Library, modules running inside the NetModel core process can use the NetModel Core Library and
   scripts can use the data model API directly (shell scripts via pcb_cli, javascript via HTTP serializer).

   The documentation of the API included herein is restricted to the details per function. Basic knowledge about NetModel is required
   to be able to read this. More high level documentation on NetModel can be found at
   http://wiki.be.softathome.com/mediawiki/index.php/Doc:NetModel_Data_Model#Architecture . On that page, some terminology
   definitions can be found as well, which are repeated here for completeness:
   - A <b>flag set</b> is a space separated list of flag names. Example: "enabled up".
   - A <b>flag expression</b> is a string in which flag names are combined with the logical operators &&, || and !.
   Subexpressions may be grouped with parentheses.
   The empty string is also a valid flag expression and it evaluates to true by definition. Example: "enabled && up".
   - Starting at a given Intf, the network stack dependency graph can be traversed in different ways. There are six predefined
   <b>traverse modes</b>:
   - <b>this</b> consider only the starting Intf.
   - <b>down</b> consider the entire closure formed by recursively following the LLIntf references.
   - <b>up</b> consider the entire closure formed by recursively following the ULIntf referenes.
   - <b>down exclusive</b> the same as down, but exclude the starting Intf.
   - <b>up exclusive</b> the same as up, but exclude the starting Intf.
   - <b>one level down</b> consider only direct LLIntfs.
   - <b>one level up</b> consider only direct ULIntfs.
   - <b>all</b> consider all Intfs.
   .
   The resulting structured set of Intfs is called the <b>traverse tree</b>.
   Example: if you apply the traverse mode "down" on Intf eth1 which has LLIntfs swport1, swport2 and swport3,
   the traverse tree will consist of eth1, swport1, swport2 and swport3.
   - Some data model functions accept a parameter and/or a function name as input argument. By extension, they may also accept a
   <b>parameter spec</b> and/or <b>function spec</b> as input argument. A parameter/function spec is the concatentation of
   the dot-separated key path relative to a NetModel Intf instance and the parameter/function name, separated by an extra dot.
   Example: the parameter spec "ReqOption.3.Value" refers to the parameter Value held by the object NetModel.Intf.{i}.ReqOption.3.

   @remarks
   Data model functions have mandatory arguments and optional arguments. The NetModel data model documentation clearly
   states which arguments are mandatory and for optional arguments, there is always a default value. More precisely, if an optional
   argument is omitted, the function behaves the exact same way as if the so called default value was provided.
   In C, optional arguments do not exist. However, passing a NULL pointer is handled the same way by the C style API
   implementation as not passing the argument at all by the data model API implementation. So for all pointer arguments of all
   functions, it is specified whether the argument is mandatory and if not, a default value is specified. Being mandatory means that
   NULL pointers are not accepted and if it's not mandatory, a NULL pointer will be handled the exact same way as the default value.

 */

#include <stdbool.h>
#include <stdint.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>
#include <amxb/amxb_types.h>

typedef void (* netmodel_callback_t)(const char* sig_name, const amxc_var_t* data, void* priv);

/**
   @brief
   Opaque handle for an opened query.

   @details
   This type of object is returned by netmodel_openQuery() and needs to be passed to netmodel_closeQuery() exactly once.
   @see netmodel_openQuery()
   @see netmodel_closeQuery()
 */
typedef struct _netmodel_query netmodel_query_t;

extern const char* const netmodel_traverse_this;           /**< String constant holding the traverse mode "this".*/
extern const char* const netmodel_traverse_down;           /**< String constant holding the traverse mode "down".*/
extern const char* const netmodel_traverse_up;             /**< String constant holding the traverse mode "up".*/
extern const char* const netmodel_traverse_down_exclusive; /**< String constant holding the traverse mode "down exclusive".*/
extern const char* const netmodel_traverse_up_exclusive;   /**< String constant holding the traverse mode "up exclusive".*/
extern const char* const netmodel_traverse_one_level_down; /**< String constant holding the traverse mode "one level down".*/
extern const char* const netmodel_traverse_one_level_up;   /**< String constant holding the traverse mode "one level up".*/
extern const char* const netmodel_traverse_all;            /**< String constant holding the traverse mode "all".*/

/**
   @brief Find a path to a leaf of the traverse tree for which each Intf is up.
   @param intf  Starting point of the traverse tree. Default is "lo".
   @param flag If non-empty, only interfaces for which this flag expression evaluates to true have to be up.
            If there is no such interface in the path, the path is not considered as a match. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return True if such path is found.
 */
bool netmodel_isUp(const char* intf, const char* flag, const char* traverse);

/**
   @brief Evaluate a flag expression on the superset of all flags of all Intfs in the traverse tree.
   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag The flag expression to evaluate. Default is empty.
   @param condition Merge only the flags of the Intfs for which this flag expression evaluates to true. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return Result of flag expression evaluation.
 */
bool netmodel_hasFlag(const char* intf, const char* flag, const char* condition, const char* traverse);

/**
   @brief Set one or more flags on all Intfs in the traverse tree.
   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag The flagset holding all flags to set. Default is empty.
   @param condition Touch only Intfs for which this flag expression evaluates to true. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "this".
 */
void netmodel_setFlag(const char* intf, const char* flag, const char* condition, const char* traverse);

/**
   @brief Clear one or more flags on all Intfs in the traverse tree.
   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag The flagset holding all flags to clear. Default is empty.
   @param condition Touch only Intfs for which this flag expression evaluates to true. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "this".
 */
void netmodel_clearFlag(const char* intf, const char* flag, const char* condition, const char* traverse);

/**
   @brief Find an Intf in the traverse tree.
   @param intf Starting point of the traverse tree. Default is "lo".
   @param target The name of the Intf to find. Mandatory.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return True if target was found.
 */
bool netmodel_isLinkedTo(const char* intf, const char* target, const char* traverse);

/**
   @brief Get the closure of the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only Intfs for which this flag expression evaluates to true are returned. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A list of Intf names.
   @warning The returned variant is allocated on the heap, so it MUST be freed by the caller of the function with amxc_var_delete().
 */
amxc_var_t* netmodel_getIntfs(const char* intf, const char* flag, const char* traverse);

/**
   @brief Get the first Intf of the traverse tree.
   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return An Intf name.
   @warning The returned string is allocated on the heap, so it MUST be freed by the caller of the function with free().
 */
char* netmodel_luckyIntf(const char* intf, const char* flag, const char* traverse);

/**
   @brief Get the values of a specific parameter of all Intfs in the traverse tree.
   @param intf Starting point of the traverse tree. Default is "lo".
   @param name Name or spec of the parameter. Mandatory.
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A variant that maps Intf names to parameter values. The parameter values are variants, their exact type depends on
        the parameter type.
   @warning The returned variant is allocated on the heap, so it MUST be freed by the caller of the function with amxc_var_delete().
 */
amxc_var_t* netmodel_getParameters(const char* intf,
                                   const char* name,
                                   const char* flag,
                                   const char* traverse);

/**
   @brief Get the value of a specific parameter of the first Intf in the traverse tree.
   @param intf Starting point of the traverse tree. Default is "lo".
   @param name Name or spec of the parameter. Mandatory.
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A parameter value. This is a variant, its exact type depends on the parameter type.
   @warning The returned variant is allocated on the heap, so it MUST be freed by the caller of the function with amxc_var_delete().
 */
amxc_var_t* netmodel_getFirstParameter(const char* intf,
                                       const char* name,
                                       const char* flag,
                                       const char* traverse);

/**
   @brief Set the values of a specific parameter of all Intfs in the traverse tree.
   @param intf Starting point of the traverse tree. Default is "lo".
   @param name Name or spec of the parameter. Mandatory.
   @param value The value to assign to the parameters. It can be any kind of variant, but preferably of the same type as the parameter.
             Mandatory.
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
 */
void netmodel_setParameters(const char* intf,
                            const char* name,
                            const amxc_var_t* value,
                            const char* flag,
                            const char* traverse);

/**
   @brief Set the value of a specific parameter of the first interface in the traverse tree.
   @param intf Starting point of the traverse tree. Default is "lo".
   @param name Name or spec of the parameter. Mandatory.
   @param value The value to assign to the parameters. It can be any kind of variant, but preferably of the same type as the parameter.
             Mandatory.
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
 */
void netmodel_setFirstParameter(const char* intf,
                                const char* name,
                                const amxc_var_t* value,
                                const char* flag,
                                const char* traverse);

/**
   @brief
   Get all active Addr objects (Enable == true) that occur in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only Addrs for which this flag expression evaluates to true are returned.
               An Addr's flag set is defined by the union of the Flags parameter, the Scope parameter and the address family
               ("ipv4" or "ipv6"), so you could call nemo_getAddrs(NULL, "ipv6 && global", "all") to get all global IPv6 addresses
               for example. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A variant that contains a list of Addr variants. Each Addr variant contains a hashtable that maps Addr parameters to their values.
   @warning The returned variant is allocated on the heap, so it MUST be freed by the caller of the function with amxc_var_delete().
 */
amxc_var_t* netmodel_getAddrs(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Get the first active Addr (Enable == true) that occurs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only Addrs for which this flag expression evaluates to true are considered.
               An Addr's flag set is defined by the union of the Flags parameter, the Scope parameter and the address family
               ("ipv4" or "ipv6"), so you could call nemo_luckyAddr(NULL, "ipv6 && global", "all") to get any global IPv6
               address for example. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A variant that maps Addr parameters to their values.
   @warning The returned variant is allocated on the heap, so it MUST be freed by the caller of the function with amxc_var_delete().
 */
amxc_var_t* netmodel_luckyAddr(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Get only the Address parameter of the first active Addr (Enable == true) that occurs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only Addrs for which this flag expression evaluates to true are considered.
               An Addr's flag set is defined by the union of the Flags parameter, the Scope parameter and the address family
               ("ipv4" or "ipv6"), so you could call nemo_luckyAddrAddress(NULL, "ipv6 && global", "all") to get any global IPv6
               address for example. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return An IP-address in standard notation.
   @warning The returned string is allocated on the heap, so it MUST be freed by the caller of the function with free().
 */
char* netmodel_luckyAddrAddress(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Finds and parses the option value for a given type and tag of any bound DHCP client Intf
   in the traverse tree. If multiple Intfs in the traverse tree contain the requested option,
   the first one will be used.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param type The type of option to be queried. Enumeration of:
               - req: DHCPv4 ReqOption
               - sent: DHCPv4 SentOption
               - req6: DHCPv6 ReceivedOption
               - sent6: DHCPv6 SentOption
               - ra: Router Advertisement Option
   @param tag The option tag
   @param traverse Traverse mode to build the traverse tree.
   @return A variant containing the parsed option value. The exact structure depends on the type and tag.
   @warning The returned variant is allocated on the heap, so it MUST be freed by the caller of the function with amxc_var_delete().
 */
amxc_var_t* netmodel_getDHCPOption(const char* intf, const char* type, uint16_t tag, const char* traverse);

/**
   @brief Get the names of the MIBs applied to all interfaces in the traverse tree.
   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "this".
   @return A variant that maps Intf names to MIB names. The MIB names are stored in a space separated string.
   @warning The returned variant is allocated on the heap, so it MUST be freed by the caller of the function with amxc_var_delete().
 */
amxc_var_t* netmodel_getMibs(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Get all active IPv6 prefix objects (Enable == true && Status == Enabled) that occur in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only prefixes for which this flag expression evaluates to true are returned.
               A prefix's flag set is defined by the union of the Origin and the StaticType parameters.
               Additionally the RAPrefix flag is set if the Origin is either Child or AutoConfigured and
               indicates this prefix should be advertised in the router advertisement messages.
               Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A variant that contains a list of Prefix variants. Each Prefix variant contains a hashtable that maps Prefix parameters to their values.
   @warning The returned variant is allocated on the heap, so it MUST be freed by the caller of the function with amxc_var_delete().
 */
amxc_var_t* netmodel_getIPv6Prefixes(const char* intf, const char* flag, const char* traverse);

/**
   @brief
   Get the first active IPv6 Prefix object (Enable == true && Status == Enabled) that occurs in the traverse tree.

   @param intf Starting point of the traverse tree. Default is "lo".
   @param flag Only prefixes for which this flag expression evaluates to true are returned.
               A prefix's flag set is defined by the union of the Origin and the StaticType parameters. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @return A variant that maps Prefix parameters to their values.
   @warning The returned variant is allocated on the heap, so it MUST be freed by the caller of the function with amxc_var_delete().
 */
amxc_var_t* netmodel_getFirstIPv6Prefix(const char* intf, const char* flag, const char* traverse);

/**
   @brief Create an upper layer Intf -> lower layer Intf dependency.
   @param ulintf The name of the upper layer Intf. Mandatory.
   @param llintf The name of the lower layer Intf. Mandatory.
 */
void netmodel_linkIntfs(const char* ulintf, const char* llintf);

/**
   @brief Delete an upper layer Intf -> lower layer Intf dependency.
   @param ulintf The name of the upper layer Intf. Mandatory.
   @param llintf The name of the lower layer Intf. Mandatory.
 */
void netmodel_unlinkIntfs(const char* ulintf, const char* llintf);

/**
   @brief Open a query to get the result of a query function and get notified whenever that result changes.

   @details
   This function is a generic method to open a query for any kind of query function. It is recommended to not use this function, but
   to use one of the query function specific wrappers instead. These wrappers do not have the args and attributes arguments to
   generically pass arguments to the query function, but they have a query function specific signature instead, which is more
   convenient.

   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param function The name of the query function (mandatory). Currently the following functions are supported:
                    - isUp
                    - hasFlag
                    - isLinkedTo
                    - getIntfs
                    - luckyIntf
                    - getFirstParameter
                    - getParameters
                    - getAddrs
                    - luckyAddr
                    - luckyAddrAddress
                    - getDHCPOption
   @param args The arguments to be passed to the query function.
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
               This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
               changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.

   @warning
   The returned pointer MUST be recorded by the caller and passed later on exactly once to netmodel_closeQuery().
   If the amx_openQuery()-call fails it will return a NULL pointer and an error will have be logged into syslog.
 */
netmodel_query_t* netmodel_openQuery(const char* intf,
                                     const char* subscriber,
                                     const char* function,
                                     const amxc_var_t* args,
                                     netmodel_callback_t handler,
                                     void* userdata);

/**
   @brief
   Close an opened query.

   @warning
   This function MUST be called exactly once for each successful call to netmodel_openQuery(). By not doing so you're not only
   leaking resources in your application, but also in the NetModel core process. Notice also that netmodel_cleanup() does NOT close queries automatically.

   @param query A NULL-pointer (NULL-pointers are ignored) or a pointer to an opaque query object returned by a successful call
             to netmodel_openQuery(). When this function returns, this pointer is no longer valid.
 */
void netmodel_closeQuery(netmodel_query_t* query);

/**
   @brief Convenience wrapper for querying the function isUp().
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param flag If non-empty, only interfaces for which this flag expression evaluates to true have to be up.
            If there is no such interface in the path, the path is not considered as a match. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_isUp(const char* intf,
                                          const char* subscriber,
                                          const char* flag,
                                          const char* traverse,
                                          netmodel_callback_t handler,
                                          void* userdata);

/**
   @brief Convenience wrapper for querying the function hasFlag().
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param flag The flag expression to evaluate. Default is empty.
   @param condition Merge only the flags of the Intfs for which this flag expression evaluates to true. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_hasFlag(const char* intf,
                                             const char* subscriber,
                                             const char* flag,
                                             const char* condition,
                                             const char* traverse,
                                             netmodel_callback_t handler,
                                             void* userdata);

/**
   @brief Convenience wrapper for querying the function isLinkedTo().
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param target The name of the Intf to find. Mandatory.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_isLinkedTo(const char* intf,
                                                const char* subscriber,
                                                const char* target,
                                                const char* traverse,
                                                netmodel_callback_t handler,
                                                void* userdata);

/**
   @brief Convenience wrapper for querying the function getIntfs().
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param flag Only Intfs for which this flag expression evaluates to true are returned. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_getIntfs(const char* intf,
                                              const char* subscriber,
                                              const char* flag,
                                              const char* traverse,
                                              netmodel_callback_t handler,
                                              void* userdata);

/**
   @brief Convenience wrapper for querying the function luckyIntf().
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_luckyIntf(const char* intf,
                                               const char* subscriber,
                                               const char* flag,
                                               const char* traverse,
                                               netmodel_callback_t handler,
                                               void* userdata);

/**
   @brief Convenience wrapper for querying the function getParameters().
   @param intf Starting point of the traverse tree. Default is "lo".
   @param subscriber An arbitrary subscriber name (mandatory).
   @param name Name or spec of the parameter. Mandatory.
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_getParameters(const char* intf,
                                                   const char* subscriber,
                                                   const char* name,
                                                   const char* flag,
                                                   const char* traverse,
                                                   netmodel_callback_t handler,
                                                   void* userdata);

/**
   @brief Convenience wrapper for querying the function getFirstParameter().
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param name Name or spec of the parameter. Mandatory.
   @param flag Only Intfs for which this flag expression evaluates to true are considered. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_getFirstParameter(const char* intf,
                                                       const char* subscriber,
                                                       const char* name,
                                                       const char* flag,
                                                       const char* traverse,
                                                       netmodel_callback_t handler,
                                                       void* userdata);

/**
   @brief Convenience wrapper for querying the function getAddrs().
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param flag Only Addrs for which this flag expression evaluates to true are returned.
               An Addr's flag set is defined by the union of the Flags parameter, the Scope parameter and the address family
               ("ipv4" or "ipv6"), so you could call nemo_getAddrs(NULL, "ipv6 && global", "all") to get all global IPv6 addresses
               for example. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_getAddrs(const char* intf,
                                              const char* subscriber,
                                              const char* flag,
                                              const char* traverse,
                                              netmodel_callback_t handler,
                                              void* userdata);

/**
   @brief Convenience wrapper for querying the function luckyAddr().
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param flag Only Addrs for which this flag expression evaluates to true are considered.
               An Addr's flag set is defined by the union of the Flags parameter, the Scope parameter and the address family
               ("ipv4" or "ipv6"), so you could call nemo_luckyAddr(NULL, "ipv6 && global", "all") to get any global IPv6
               address for example. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_luckyAddr(const char* intf,
                                               const char* subscriber,
                                               const char* flag,
                                               const char* traverse,
                                               netmodel_callback_t handler,
                                               void* userdata);

/**
   @brief Convenience wrapper for querying the function luckyAddrAddress().
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param flag Only Addrs for which this flag expression evaluates to true are considered.
               An Addr's flag set is defined by the union of the Flags parameter, the Scope parameter and the address family
               ("ipv4" or "ipv6"), so you could call nemo_luckyAddrAddress(NULL, "ipv6 && global", "all") to get any global IPv6
               address for example. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_luckyAddrAddress(const char* intf,
                                                      const char* subscriber,
                                                      const char* flag,
                                                      const char* traverse,
                                                      netmodel_callback_t handler,
                                                      void* userdata);

/**
   @brief Convenience wrapper for querying the function getDHCPOption()
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param type The type of option to be queried. Enumeration of:
               - req: DHCPv4 ReqOption
               - sent: DHCPv4 SentOption
               - req6: DHCPv6 ReceivedOption
               - sent6: DHCPv6 SentOption
               - ra: Router Advertisement Option
   @param tag The option tag
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_getDHCPOption(const char* intf,
                                                   const char* subscriber,
                                                   const char* type,
                                                   uint16_t tag,
                                                   const char* traverse,
                                                   netmodel_callback_t handler,
                                                   void* userdata);
/**
   @brief Convenience wrapper for querying the function getIPv6Prefixes().
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param flag Only prefixes for which this flag expression evaluates to true are returned.
               A prefix's flag set is defined by the union of the Origin and the StaticType parameters.
               Additionally the RAPrefix flag is set if the Origin is either Child or AutoConfigured and
               indicates this prefix should be advertised in the router advertisement messages.
               Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_getIPv6Prefixes(const char* intf,
                                                     const char* subscriber,
                                                     const char* flag,
                                                     const char* traverse,
                                                     netmodel_callback_t handler,
                                                     void* userdata);

/**
   @brief Convenience wrapper for querying the function getFirstIPv6Prefix().
   @param intf The interface to open the query on. This corresponds to the first argument of the actual query function.
   @param subscriber An arbitrary subscriber name (mandatory).
   @param flag Only prefixes for which this flag expression evaluates to true are returned.
               A prefix's flag set is defined by the union of the Origin and the StaticType parameters. Default is empty.
   @param traverse Traverse mode to build the traverse tree. Default is "down".
   @param handler The callback function to be called with the initial query result and later on whenever the query result changes.
           This function will be called for the first time before the call to netmodel_openQuery() finishes. If the query result
           changes later on, the function will be called from within the event loop.
   @param userdata A void pointer to be passed to the handler.
   @return A pointer to an opaque query object if the openQuery()-call succeeded.
 */
netmodel_query_t* netmodel_openQuery_getFirstIPv6Prefix(const char* intf,
                                                        const char* subscriber,
                                                        const char* flag,
                                                        const char* traverse,
                                                        netmodel_callback_t handler,
                                                        void* userdata);

/**
   @}
 */

#endif /* __NETMODEL_COMMON_API_H__ */
